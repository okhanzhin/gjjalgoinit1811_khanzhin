package util;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.Arrays;

public class Assert {

    public static void assertEquals(String testName, int expected, int actual) {
        if (expected == actual) {
            System.out.println(testName + " passed");
        } else {
            System.out.println(testName + " failed: expected " + expected + ", actual " + actual);
        }
    }

    public static void assertEquals(String testName, long expected, long actual) {
        if (expected == actual) {
            System.out.println(testName + " passed");
        } else {
            System.out.println(testName + " failed: expected " + expected + ", actual " + actual);
        }
    }


    public static void assertEquals(String testName, float expected, float actual) {
        if (expected == actual) {
            System.out.println(testName + " passed");
        } else {
            System.out.println(testName + " failed: expected " + expected + ", actual " + actual);
        }
    }

    public static void assertEquals(String testName, double expected, double actual) {
        if (expected == actual) {
            System.out.println(testName + " passed");
        } else {
            System.out.println(testName + " failed: expected " + expected + ", actual " + actual);
        }
    }

    public static void assertEquals(String testName, char expected, char actual) {
        if (expected == actual) {
            System.out.println(testName + " passed");
        } else {
            System.out.println(testName + " failed: expected " + expected + ", actual " + actual);
        }
    }

    public static void assertBoolean(String testName, boolean expected, boolean actual) {
        if (expected == actual) {
            System.out.println(testName + " passed");
        } else {
            System.out.println(testName + " failed: expected " + expected + ", actual " + actual);
        }
    }

    public static void assertObject(String testName, Object expected, Object actual) {
        if (actual.equals(expected)) {
            System.out.println(testName + " passed");
        } else {
            System.out.println(testName + " failed: expected " + expected + ", actual " + actual);
        }
    }

    public static void assertStringBuilder(String testName, StringBuilder expected, StringBuilder actual) {
        if (expected.toString().equals(actual.toString())) {
            System.out.println(testName + " passed");
        } else {
            System.out.println(testName + " failed: expected " + expected + ", actual " + actual);
        }
    }

    public static void assertEquals(String testName, double[] expected, double[] actual) {
        if (Arrays.equals(expected, actual)) {
            System.out.println(testName + " passed");
        } else {
            System.out.println(testName + " failed: expected \n" +
                Arrays.toString(expected) + "\n actual \n" + Arrays.toString(actual));
        }
    }

    public static void assertEquals(String testName, int[] expected, int[] actual) {
        if (Arrays.equals(expected, actual)) {
            System.out.println(testName + " passed");
        } else {
            System.out.println(testName + " failed: expected \n" +
                Arrays.toString(expected) + "\n actual \n" + Arrays.toString(actual));
        }
    }

    public static void assertEquals(String testName, int[][] expected, int[][] actual) {
        if (Arrays.deepEquals(expected, actual)) {
            System.out.println(testName + " passed");
        } else {
            System.out.println(testName + " failed: expected \n" +
                Arrays.deepToString(expected) + "\n actual \n" + Arrays.deepToString(actual));
        }
    }

    public static void assertFile(String testName, String expected, String actual) {
        File f2 = new File("");

        try {
            BufferedReader r1 = new BufferedReader(new FileReader(f2.getAbsolutePath() + expected));
            BufferedReader r2 = new BufferedReader(new FileReader(f2.getAbsolutePath() + actual));

            String line1;
            String line2;

            while ((line1 = r1.readLine()) != null && (line2 = r2.readLine()) != null) {
                if (!line1.trim().equals(line2.trim())) {
                    System.out.println(testName + " \nfailed: expected " + line1 + "\n, actual " + line2);

                    return;
                }
            }

            System.out.println(testName + " passed");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}