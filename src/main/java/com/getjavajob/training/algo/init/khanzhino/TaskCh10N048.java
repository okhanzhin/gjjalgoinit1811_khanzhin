package com.getjavajob.training.algo.init.khanzhino;

import java.util.Arrays;
import java.util.Scanner;
import static java.lang.Math.random;

public class TaskCh10N048 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int numberOfSequenceElements = sc.nextInt();

        int[] specifiedArray = new int[numberOfSequenceElements];

        for (int i = 0; i < specifiedArray.length; i++) {
            specifiedArray[i] = (int) (random() * numberOfSequenceElements);
        }

        System.out.println(Arrays.toString(specifiedArray));
        System.out.println(getMaxElementOfArray(specifiedArray, numberOfSequenceElements));
    }

    public static int getMaxElementOfArray(int[] specifiedArray, int numberOfSequenceElements) {
        if (numberOfSequenceElements == 1) {
            return specifiedArray[0];
        } else {
            return chooseMax(getMaxElementOfArray(specifiedArray, numberOfSequenceElements - 1),
                                                                          specifiedArray[numberOfSequenceElements - 1]);
        }
    }

    public static int chooseMax(int number1, int number2) {
        return number1 > number2 ? number1 : number2;
    }
}
