package com.getjavajob.training.algo.init.khanzhino;

import java.util.Scanner;

public class TaskCh09N185 {
    public static void main(String[] args) {
        String inputString = "";

        Scanner sc = new Scanner(System.in);
        inputString = sc.nextLine();

        System.out.println(checkCorrectPlaceOfParentheses(inputString));
        System.out.println(checkCorrectPlaceOfParenthesesWithComment(inputString));
    }

    public static boolean checkCorrectPlaceOfParentheses(String inputString) {
        int leftCounter = 0;
        int rightCounter = 0;
        boolean validResult = false;

        for (int i = 0; i < inputString.length(); i++) {
            if (inputString.charAt(i) == '(') {
                leftCounter++;
            } else if (inputString.charAt(i) == ')') {
                rightCounter++;
            }
        }

        if (leftCounter == rightCounter) {
            validResult = true;
        }

        return validResult;
    }

    public static String checkCorrectPlaceOfParenthesesWithComment(String inputString) {
        int leftCounter = 0;
        String result = "";
        StringBuilder resultBuffer = new StringBuilder();

        for (int i = 0; i < inputString.length(); i++) {
            switch (inputString.charAt(i)) {
                case '(':
                    leftCounter++;
                    break;
                case ')':
                    if (leftCounter != 0) {
                        leftCounter--;
                    } else {
                        result = resultBuffer.append("No. Extra right bracket with index ").append(i + 1).append(".").toString();
                    }
                    break;
            }
        }

        if (leftCounter != 0) {
            result = "No. Number of extra opening brackets is " + leftCounter + ".";
        } else if (result.isEmpty()) {
            result = "All brackets are arranged correctly.";
        }

        return result;
    }
}
