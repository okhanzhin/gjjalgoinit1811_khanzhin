package com.getjavajob.training.algo.init.khanzhino;

import static com.getjavajob.training.algo.init.khanzhino.TaskCh12N024.getSolutionA;
import static com.getjavajob.training.algo.init.khanzhino.TaskCh12N024.getSolutionB;
import static util.Assert.assertEquals;

public class TaskCh12N024Test {
    public static void main(String[] args) {
        testSolutionA();
        testSolutionB();
    }

    public static void testSolutionA() {
        assertEquals("TaskCh12N024Test.testSolutionA", new int[][]{
                        {1, 1, 1, 1, 1, 1},
                        {1, 2, 3, 4, 5, 6},
                        {1, 3, 6, 10, 15, 21},
                        {1, 4, 10, 20, 35, 56},
                        {1, 5, 15, 35, 70, 126},
                        {1, 6, 21, 56, 126, 252}},
                        getSolutionA(6));
    }

    public static void testSolutionB() {
        assertEquals("TaskCh12N024Test.testSolutionB", new int[][]{
                        {1, 2, 3, 4, 5, 6},
                        {2, 3, 4, 5, 6, 1},
                        {3, 4, 5, 6, 1, 2},
                        {4, 5, 6, 1, 2, 3},
                        {5, 6, 1, 2, 3, 4},
                        {6, 1, 2, 3, 4, 5}},
                        getSolutionB(6));
    }
}
