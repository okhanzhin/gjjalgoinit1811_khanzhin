package com.getjavajob.training.algo.init.khanzhino;

import static com.getjavajob.training.algo.init.khanzhino.TaskCh10N052.getReverseNumber;
import static util.Assert.assertEquals;

public class TaskCh10N052Test {
    public static void main(String[] args) {
        testReverseNumber();
    }

    public static void testReverseNumber() {
        assertEquals("TaskCh10N052Test.testReverseNumber", 97531,
                                getReverseNumber(13579));
    }
}
