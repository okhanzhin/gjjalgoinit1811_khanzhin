package com.getjavajob.training.algo.init.khanzhino;

import java.util.Scanner;

public class TaskCh04N033 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int inputNumber;

        do {
            System.out.println("Type an natural number: ");
            inputNumber = sc.nextInt();
        } while (inputNumber < 1);

        System.out.println(checkEven(inputNumber));
    }

    public static boolean checkEven(int naturalNumber) {
        return naturalNumber % 2 == 0;
    }

    public static boolean checkOdd(int naturalNumber) {
        return naturalNumber % 2 != 0;
    }
}
