package com.getjavajob.training.algo.init.khanzhino;

import static com.getjavajob.training.algo.init.khanzhino.TaskCh05N010.getExchangeTable;
import static util.Assert.assertEquals;

public class TaskCh05N010Test {
    public static void main(String[] args) {
        testExchangeTable();
    }

    public static void testExchangeTable() {
        assertEquals("TaskCh05N010Test.testExchangeTable", new double[]{
                                                                    66.0, 132.0, 198.0,
                                                                    264.0, 330.0, 396.0,
                                                                    462.0, 528.0, 594.0,
                                                                    660.0, 726.0, 792.0,
                                                                    858.0, 924.0, 990.0,
                                                                    1056.0, 1122.0, 1188.0,
                                                                    1254.0, 1320.0},
                                                                    getExchangeTable(66.0));
    }
}