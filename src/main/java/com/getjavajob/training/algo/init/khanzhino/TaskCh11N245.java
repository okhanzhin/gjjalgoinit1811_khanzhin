package com.getjavajob.training.algo.init.khanzhino;

import java.util.Arrays;

public class TaskCh11N245 {
    public static void main(String[] args) {
        int[] inputArray = {-1, 5, 2, -6, 5, 10, 23, -13, 9};;

        System.out.println(Arrays.toString(inputArray));
        System.out.println(Arrays.toString(getNegativeFirst(inputArray)));
    }

    public static int[] getNegativeFirst(int[] inputArray) {
        int[] negativeElements = new int[inputArray.length];
        int[] positiveElements = new int[inputArray.length];

        int negativeCount = 0;
        int positiveCount = 0;

        for (int i = 0; i < inputArray.length; i++) {
            if (inputArray[i] < 0) {
                negativeElements[negativeCount++] = inputArray[i];
            } else {
                positiveElements[positiveCount++] = inputArray[i];
            }
        }

        for (int i = 0; i < positiveCount; i++) {
            negativeElements[negativeCount + i] = positiveElements[i];
        }

        return negativeElements;
    }
}


