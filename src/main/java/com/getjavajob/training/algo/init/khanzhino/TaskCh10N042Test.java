package com.getjavajob.training.algo.init.khanzhino;

import static com.getjavajob.training.algo.init.khanzhino.TaskCh10N042.raiseNumberToPower;
import static util.Assert.assertEquals;

public class TaskCh10N042Test {

    public static void main(String[] args) {
        testRaiseNumberToPower();
    }

    public static void testRaiseNumberToPower() {
        assertEquals("TaskCh10N042Test.testRaiseNumberToPower", 27.0,
                                raiseNumberToPower(3, 3));
    }
}
