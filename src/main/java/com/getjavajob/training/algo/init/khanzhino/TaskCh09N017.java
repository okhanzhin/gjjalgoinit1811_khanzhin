package com.getjavajob.training.algo.init.khanzhino;

import java.util.Scanner;

public class TaskCh09N017 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        String inputWord = sc.next();

        System.out.println(checkIdentityOfCharacters(inputWord));
    }

    public static String checkIdentityOfCharacters(String inputWord) {
        char firstCharacter = inputWord.charAt(0);
        char lastCharacter = inputWord.charAt(inputWord.length() - 1);

        return String.valueOf(firstCharacter).equalsIgnoreCase(String.valueOf(lastCharacter)) ?
                             "Letters are identical." : "Letters aren't the same.";
    }
}
