package com.getjavajob.training.algo.init.khanzhino;

import java.util.Scanner;

public class TaskCh09N042 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        String inputWord = sc.next();

        System.out.println(reverseInputString(inputWord));
    }

    public static StringBuilder reverseInputString(String inputWord) {
        return new StringBuilder(inputWord).reverse();
    }
}
