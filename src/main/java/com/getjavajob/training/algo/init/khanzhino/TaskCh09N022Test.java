package com.getjavajob.training.algo.init.khanzhino;

import static com.getjavajob.training.algo.init.khanzhino.TaskCh09N022.getHalfAWord;
import static util.Assert.assertObject;

public class TaskCh09N022Test {
    public static void main(String[] args) {
        testDividingWord();
    }

    public static void testDividingWord() {
        assertObject("TaskCh09N022Test.testDividingWord", "num", getHalfAWord("number"));
    }
}
