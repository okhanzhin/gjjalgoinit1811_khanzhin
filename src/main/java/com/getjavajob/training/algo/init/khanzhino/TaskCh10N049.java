package com.getjavajob.training.algo.init.khanzhino;

import java.util.Arrays;
import java.util.Scanner;
import static java.lang.Math.random;

public class TaskCh10N049 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int numberOfSequenceElements = sc.nextInt();

        int[] specifiedArray = new int[numberOfSequenceElements];

        for (int i = 0; i < specifiedArray.length; i++) {
            specifiedArray[i] = (int) (random() * numberOfSequenceElements);
        }

        System.out.println(Arrays.toString(specifiedArray));
        System.out.println(getIndexOfMaxElement(specifiedArray, 0));
    }

    public static int getIndexOfMaxElement(int[] specifiedArray, int currentMaxIndex) {
        if (currentMaxIndex == specifiedArray.length - 1) {
            return currentMaxIndex;
        }

        int currentIndex = getIndexOfMaxElement(specifiedArray, currentMaxIndex + 1);

        return specifiedArray[currentMaxIndex] > specifiedArray[currentIndex] ? currentMaxIndex : currentIndex;
    }
}
