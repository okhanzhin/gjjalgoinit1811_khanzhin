package com.getjavajob.training.algo.init.khanzhino;

import static com.getjavajob.training.algo.init.khanzhino.TaskCh04N106.determineSeason;
import static util.Assert.assertObject;

public class TaskCh04N106Test {
    public static void main(String[] args) {
        testWinter();
        testSpring();
        testSummer();
        testAutumn();
        testIncorrectMonth();
    }

    public static void testWinter() {
        assertObject("TaskCh04N106Test.testWinter", "winter",
                                determineSeason(1));
    }

    public static void testSpring() {
        assertObject("TaskCh04N106Test.testSpring", "spring",
                                determineSeason(4));
    }

    public static void testSummer() {
        assertObject("TaskCh04N106Test.testSummer", "summer",
                                determineSeason(6));
    }

    public static void testAutumn() {
        assertObject("TaskCh04N106Test.testAutumn", "autumn",
                                determineSeason(10));
    }

    public static void testIncorrectMonth() {
        assertObject("TaskCh04N106Test.testIncorrectMonth", "Enter the month number correctly!",
                                determineSeason(20));
    }
}
