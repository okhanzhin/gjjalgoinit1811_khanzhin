package com.getjavajob.training.algo.init.khanzhino;

import java.util.Scanner;

public class TaskCh10N044 {
    private static final int TENS = 10;

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int inputNumber = sc.nextInt();

        System.out.println(getFinalRoot(inputNumber));
    }

    public static int getDigitalRoot(int inputNumber) {
        if (inputNumber == 0) {
            return inputNumber;
        } else {
            return inputNumber % TENS + getDigitalRoot(inputNumber / TENS);
        }
    }

    public static int getFinalRoot(int inputNumber) {
        if (getDigitalRoot(inputNumber) / TENS == 0) {
            return getDigitalRoot(inputNumber);
        } else {
            return getFinalRoot(getDigitalRoot(inputNumber));
        }
    }
}
