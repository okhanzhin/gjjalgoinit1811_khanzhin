package com.getjavajob.training.algo.init.khanzhino;

import static com.getjavajob.training.algo.init.khanzhino.TaskCh02N043.verifyMultiplicityAToB;
import static util.Assert.assertEquals;

public class TaskCh02N043Test {
    public static void main(String[] args) {
        testMultiplicityAccept();
        testMultiplicityFailed();
    }

    public static void testMultiplicityAccept() {
        assertEquals("TaskCh02N043Test.testMultiplicityAccept", 1,
                                verifyMultiplicityAToB(4, 2));
    }

    public static void testMultiplicityFailed() {
        assertEquals("TaskCh02N043Test.testMultiplicityFailed", 11,
                                verifyMultiplicityAToB(5, 17));
    }
}
