package com.getjavajob.training.algo.init.khanzhino;

import static com.getjavajob.training.algo.init.khanzhino.TaskCh10N046.getGeometricMember;
import static com.getjavajob.training.algo.init.khanzhino.TaskCh10N046.sumGeometricMembers;
import static util.Assert.assertEquals;

public class TaskCh10N046Test {
    public static void main(String[] args) {
        testGeometricMember();
        testSumOfArithmeticNMembers();
    }

    public static void testGeometricMember(){
        assertEquals("TaskCh10N046Test.testGeometricMember", 18.0,
                      getGeometricMember(2, 3, 3));
    }

    public static void testSumOfArithmeticNMembers() {
        assertEquals("TaskCh10N046Test.testSumOfArithmeticNMembers", 26.0,
                      sumGeometricMembers(2, 3, 3));
    }
}
