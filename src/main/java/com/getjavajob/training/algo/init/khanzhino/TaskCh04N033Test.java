package com.getjavajob.training.algo.init.khanzhino;

import static com.getjavajob.training.algo.init.khanzhino.TaskCh04N033.checkEven;
import static com.getjavajob.training.algo.init.khanzhino.TaskCh04N033.checkOdd;
import static util.Assert.assertBoolean;

public class TaskCh04N033Test {
    public static void main(String[] args) {
        testEvenAccept();
        testEvenFailed();
        testOddAccept();
        testOddFailed();
    }

    public static void testEvenAccept() {
        assertBoolean("TaskCh04N033Test.testEvenAccept", true,
                                checkEven(8));
    }

    public static void testEvenFailed() {
        assertBoolean("TaskCh04N033Test.testEvenFailed", false,
                                checkEven(5));
    }

    public static void testOddAccept() {
        assertBoolean("TaskCh04N033Test.testOddAccept", true,
                                checkOdd(9));
    }

    public static void testOddFailed() {
        assertBoolean("TaskCh04N033Test.testOddFailed", false,
                                checkOdd(4));
    }
}
