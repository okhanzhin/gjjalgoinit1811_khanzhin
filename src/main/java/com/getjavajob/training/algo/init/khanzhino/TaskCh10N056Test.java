package com.getjavajob.training.algo.init.khanzhino;

import static com.getjavajob.training.algo.init.khanzhino.TaskCh10N056.checkNumberIsPrime;
import static util.Assert.assertBoolean;

public class TaskCh10N056Test {
    public static void main(String[] args) {
        testPrimeNumberAccept();
        testPrimeNumberFailed();
        testPrimeNegativeNumberFailed();
    }

    public static void testPrimeNumberAccept() {
        assertBoolean("TaskCh10N056.testPrimeNumberAccept", true,
                                 checkNumberIsPrime(3, 2));
    }

    public static void testPrimeNumberFailed() {
        assertBoolean("TaskCh10N056.testPrimeNumberFailed", false,
                                 checkNumberIsPrime(16, 2));
    }

    public static void testPrimeNegativeNumberFailed() {
        assertBoolean("TaskCh10N056.testPrimeNumberFailed", false,
                                 checkNumberIsPrime(-3, 2));
    }
}
