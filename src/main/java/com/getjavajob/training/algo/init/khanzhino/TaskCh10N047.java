package com.getjavajob.training.algo.init.khanzhino;

import java.util.Scanner;

public class TaskCh10N047 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int requiredNumber = sc.nextInt();

        System.out.println(getFibonacciNumber(requiredNumber));
    }

    public static long getFibonacciNumber(int requiredNumber) {
        if (requiredNumber == 0) {
            return 0;
        } else if (requiredNumber <= 2) {
            return 1;
        } else {
            return getFibonacciNumber(requiredNumber - 1) +
                    getFibonacciNumber(requiredNumber - 2);
        }
    }
}
