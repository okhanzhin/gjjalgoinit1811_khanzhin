package com.getjavajob.training.algo.init.khanzhino;

import static com.getjavajob.training.algo.init.khanzhino.TaskCh06N008.calculateSeriesOfPossibleNumbers;
import static util.Assert.assertEquals;

public class TaskCh06N008Test {
    public static void main(String[] args) {
        testSeriesOfPossibleNumbers();
    }

    public static void testSeriesOfPossibleNumbers() {
        assertEquals("TaskCh06N008Test.testSeriesOfPossibleNumbers", new int[]{1, 4, 9},
                                calculateSeriesOfPossibleNumbers(10));
    }
}
