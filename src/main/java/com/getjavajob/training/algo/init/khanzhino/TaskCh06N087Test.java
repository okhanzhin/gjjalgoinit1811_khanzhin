package com.getjavajob.training.algo.init.khanzhino;

import static util.Assert.assertObject;

public class TaskCh06N087Test {
    public static void main(String[] args) {
        Game game = new Game();
        game.setTeam1("team1");
        game.setTeam2("team2");
        assertObject("score", game.score(), "team1 0 : 0 team2");

        game.setScore1(13);
        game.setScore2(15);
        assertObject("result team 2 winner", game.result(),
                        "WINNER " + game.getTeam2() + " " + game.getScore2() + " - " +
                                game.getScore1() + " " + game.getTeam1());

        game.setScore1(10);
        game.setScore2(15);
        assertObject("result team 1 winner", game.result(),
                        "WINNER " + game.getTeam1() + " " + game.getScore1() + " - " +
                                game.getScore2() + " " + game.getTeam2());

        game.setScore1(15);
        game.setScore2(15);
        assertObject("result draw", game.result(),
                        "DRAW" + game.getTeam1() + " " + game.getScore1() + " - " +
                                game.getScore2() + " " + game.getTeam2());
    }
}
