package com.getjavajob.training.algo.init.khanzhino;

import java.util.Arrays;
import java.util.Scanner;

public class TaskCh10N055 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int inputNumber = sc.nextInt();
        int baseOfNumeralSystem = sc.nextInt();

        while (baseOfNumeralSystem < 2 || baseOfNumeralSystem > 16) {
            System.out.println("Enter permissible base of Numeral System (2 <= N <= 16)");
            baseOfNumeralSystem = sc.nextInt();
        }

        getConversionResult(inputNumber, baseOfNumeralSystem);
    }

    public static int[] transferToAnotherNumberSystem(int inputNumber, int baseOfNumeralSystem) {

        int intermediateResult = inputNumber / baseOfNumeralSystem;
        int[] outputArray;

        if (intermediateResult >= baseOfNumeralSystem) {
            outputArray = transferToAnotherNumberSystem(intermediateResult, baseOfNumeralSystem);
        } else {
            outputArray = new int[2];

            outputArray[0] = inputNumber / baseOfNumeralSystem;
            outputArray[1] = inputNumber % baseOfNumeralSystem;


            while (outputArray.length != 1 && outputArray[0] == 0) {
                int[] finalArray = new int[outputArray.length - 1];
                for (int i = 0; i < finalArray.length; i++) {
                    finalArray[i] = outputArray[i + 1];
                }
                outputArray = finalArray;
            }

            return outputArray;
        }

        int[] outputArray2 = new int[outputArray.length + 1];

        for (int i = 0; i < outputArray.length; i++) {
            outputArray2[i] = outputArray[i];
        }

        outputArray2[outputArray.length] = inputNumber % baseOfNumeralSystem;

        return outputArray2;
    }

    public static String getConversionResult(int inputNumber, int baseOfNumeralSystem) {
        if (inputNumber < 0) {
            return "Error: input number out of range.";
        } else if (baseOfNumeralSystem < 2 || baseOfNumeralSystem > 16) {
            return "Error: base out of bounds.";
        }

        int[] resultArray = transferToAnotherNumberSystem(inputNumber, baseOfNumeralSystem);

        System.out.println(Arrays.toString(resultArray));
        char[] resultCharArray = new char[resultArray.length];

        for (int i = 0; i < resultArray.length; i++) {
            if (resultArray[i] >= 0 && resultArray[i] <= 9) {
                resultCharArray[i] = (char) (resultArray[i] + 48);
            } else {
                resultCharArray[i] = (char) (resultArray[i] + 55);
            }
        }

        StringBuilder outputBuffer = new StringBuilder();

        for (int i = 0; i < resultCharArray.length; i++) {
            outputBuffer.append(resultCharArray[i]);
        }

        return outputBuffer.toString();
    }
}