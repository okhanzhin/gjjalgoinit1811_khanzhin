package com.getjavajob.training.algo.init.khanzhino;

import java.util.Scanner;

public class TaskCh10N041 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int inputNumber = sc.nextInt();

        System.out.println(calculateFactorial(inputNumber));
    }

    public static int calculateFactorial(int inputNumber) {
        return inputNumber == 1 ? inputNumber : inputNumber * calculateFactorial(inputNumber - 1);
    }
}
