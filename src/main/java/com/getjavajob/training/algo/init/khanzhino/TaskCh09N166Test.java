package com.getjavajob.training.algo.init.khanzhino;

import static com.getjavajob.training.algo.init.khanzhino.TaskCh09N166.swapFirstAndLastWord;
import static util.Assert.assertObject;

public class TaskCh09N166Test {
    public static void main(String[] args) {
        testSwappingWords();
        testSwappingWordsWithComma();
    }

    public static void testSwappingWordsWithComma() {
        assertObject("TaskCh09N166Test.testSwappingWordsWithComma",
                     "Way, is a great day, by the today.",
                      swapFirstAndLastWord("Today, is a great day, by the way."));
    }

    public static void testSwappingWords() {
        assertObject("TaskCh09N166Test.testSwappingWords",
                     "Way is a great day, by the today.",
                      swapFirstAndLastWord("Today is a great day, by the way."));
    }
}
