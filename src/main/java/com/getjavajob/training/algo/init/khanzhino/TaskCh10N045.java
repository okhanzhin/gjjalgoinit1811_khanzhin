package com.getjavajob.training.algo.init.khanzhino;

import java.util.Scanner;

public class TaskCh10N045 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        double initialValue = sc.nextDouble();
        double stepOfProgression = sc.nextDouble();
        int numberRequiredMember = sc.nextInt();

        System.out.println(getArithmeticMember(initialValue, stepOfProgression, numberRequiredMember));
        System.out.println(sumArithmeticMembers(initialValue, stepOfProgression, numberRequiredMember));
    }

    public static double getArithmeticMember(double initialValue, double stepOfProgression, int numberRequiredMember) {
        if (numberRequiredMember == 1) {
            return initialValue;
        } else {
            return getArithmeticMember(initialValue, stepOfProgression,
                    numberRequiredMember - 1) + stepOfProgression;
        }
    }

    public static double sumArithmeticMembers(double initialValue, double stepOfProgression, int numberRequiredMember) {
        if (numberRequiredMember == 1) {
            return initialValue;
        } else {
            return initialValue + stepOfProgression * (numberRequiredMember - 1) +
                    sumArithmeticMembers(initialValue, stepOfProgression, numberRequiredMember - 1);
        }
    }
}
