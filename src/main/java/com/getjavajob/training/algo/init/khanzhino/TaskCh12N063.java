package com.getjavajob.training.algo.init.khanzhino;

import java.util.Arrays;

public class TaskCh12N063 {
    public static void main(String[] args) {
        int[][] studentNumberArray = {{19, 50, 16, 18},
                                      {23, 20, 19, 15},
                                      {23, 20, 19, 15},
                                      {23, 20, 19, 15},
                                      {23, 20, 19, 15},
                                      {23, 20, 19, 15},
                                      {5, 10, 15, 17},
                                      {23, 20, 19, 15},
                                      {23, 20, 19, 15},
                                      {23, 20, 19, 15},
                                      {23, 20, 19, 15}};

        System.out.println(Arrays.toString(getAverageInParallel(studentNumberArray)));
    }

    public static int[] getAverageInParallel(int[][] studentNumberArray) {
        int[] averageArray = new int[studentNumberArray.length];
        int studentSum = 0;

        for (int i = 0; i < studentNumberArray.length; i++) {
            for (int j = 0; j < studentNumberArray[0].length; j++) {
                studentSum += studentNumberArray[i][j];
                averageArray[i] = studentSum / studentNumberArray[i].length;
            }

            studentSum = 0;
        }

        return averageArray;
    }
}
