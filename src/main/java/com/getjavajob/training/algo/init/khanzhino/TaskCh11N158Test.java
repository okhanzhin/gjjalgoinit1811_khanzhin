package com.getjavajob.training.algo.init.khanzhino;

import static com.getjavajob.training.algo.init.khanzhino.TaskCh11N158.removeTheSameElement;
import static util.Assert.*;

public class TaskCh11N158Test {
    public static void main(String[] args) {
        testRemovingSameElements();
    }

    public static void testRemovingSameElements() {
        assertEquals("TaskCh11N158Test.testRemovingSameElements",
                                new int[]{1, 3, 4, 5, 6, 7, 8, 10, 0, 0, 0, 0, 0},
                                removeTheSameElement(new int[]{1, 1, 3, 4, 5, 6, 6, 7, 7, 7, 8, 8, 10}));
    }
}
