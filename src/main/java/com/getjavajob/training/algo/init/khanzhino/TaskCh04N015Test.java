package com.getjavajob.training.algo.init.khanzhino;

import static com.getjavajob.training.algo.init.khanzhino.TaskCh04N015.calculateAge;
import static util.Assert.assertEquals;

public class TaskCh04N015Test {
    public static void main(String[] args) {
        testResultAge1();
        testResultAge2();
        testResultAge3();
    }

    public static void testResultAge1() {
        assertEquals("TaskCh04N015Test.testResultAge1", 29,
                                calculateAge(6, 1985, 12, 2014));
    }

    public static void testResultAge2() {
        assertEquals("TaskCh04N015Test.testResultAge2", 28,
                                calculateAge(6, 1985, 5, 2014));
    }

    public static void testResultAge3() {
        assertEquals("TaskCh04N015Test.testResultAge3", 29,
                                calculateAge(6, 1985, 6 , 2014));
    }
}
