package com.getjavajob.training.algo.init.khanzhino;

import java.util.Scanner;

public class TaskCh02N043 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int a = sc.nextInt();
        int b = sc.nextInt();

        System.out.println(verifyMultiplicityAToB(a, b));
    }

    public static int verifyMultiplicityAToB(int a, int b) {
        return (a % b) * (b % a) + 1;
    }
}
