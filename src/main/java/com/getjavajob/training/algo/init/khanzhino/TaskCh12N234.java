package com.getjavajob.training.algo.init.khanzhino;

import java.util.Scanner;

public class TaskCh12N234 {
    public static void main(String[] args) {
        int[][] workArrayForLineDeletion = {{19, 50, 16, 18},
                                            {23, 20, 19, 15},
                                            {123, 200, 1000, 160},
                                            {23, 20, 19, 15}};

        int[][] workArrayForColumnDeletion = {{19, 50, 16, 18},
                                              {23, 20, 19, 15},
                                              {123, 200, 1000, 160},
                                              {23, 20, 19, 15}};

        Scanner sc = new Scanner(System.in);
        int deletedElement = sc.nextInt();

        printArray(deleteLine(deletedElement, workArrayForLineDeletion));
        printArray(deleteColumn(deletedElement, workArrayForColumnDeletion));
    }

    public static int[][] deleteLine(int deletedElement, int[][] workArray) {
        for (int i = deletedElement - 1; i < workArray.length - 1; i++) {
            for (int j = 0; j <= workArray[0].length - 1; j++) {
                workArray[i][j] = workArray[i + 1][j];
            }
        }

        for (int i = workArray.length - 1; ; ) {
            for (int j = 0; j < workArray[0].length; j++) {
                workArray[i][j] = 0;
            }

            return workArray;
        }
    }

    public static int[][] deleteColumn(int deletedElement, int[][] workArray) {
        for (int i = 0; i <= workArray.length - 1; i++) {
            for (int j = deletedElement - 1; j < workArray[0].length - 1; j++) {
                workArray[i][j] = workArray[i][j + 1];
            }
        }

        for (int j = workArray[0].length - 1; ; ) {
            for (int i = 0; i < workArray.length; i++) {
                workArray[i][j] = 0;
            }

            return workArray;
        }
    }

    public static void printArray(int[][] resultArray) {
        StringBuilder printBuffer = new StringBuilder();

        for (int i = 0; i < resultArray.length; i++) {
            for (int j = 0; j < resultArray[0].length; j++) {
                System.out.print(printBuffer.append(resultArray[i][j]).append("\t").toString());
                printBuffer.delete(0, printBuffer.length());
            }
            System.out.println();
        }
        System.out.println();
    }
}
