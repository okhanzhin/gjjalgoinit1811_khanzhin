package com.getjavajob.training.algo.init.khanzhino;

import java.util.Scanner;

public class TaskCh12N024 {

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();

        printArray(getSolutionA(n));
        printArray(getSolutionB(n));
    }

    public static int[][] getSolutionA(int n) {
        int[][] outputArray = new int[n][n];

        for (int i = 0; i < outputArray.length; i++) {
            for (int j = 0; j < outputArray[i].length; j++) {
                if (i == 0 || j == 0) {
                    outputArray[i][j] = 1;
                } else {
                    outputArray[i][j] = outputArray[i - 1][j] + outputArray[i][j - 1];
                }
            }
        }

        return outputArray;
    }

    public static int[][] getSolutionB(int n) {
        int[][] outputArray = new int[n][n];

        for (int i = 0; i < outputArray.length; i++) {
            for (int j = 0; j < outputArray[i].length; j++) {
                outputArray[i][j] = (i + j) % outputArray.length + 1;
            }
        }

        return outputArray;
    }

    public static void printArray(int[][] resultArray) {
        StringBuilder printBuffer = new StringBuilder();

        for (int i = 0; i < resultArray.length; i++) {
            for (int j = 0; j < resultArray[0].length; j++) {
                System.out.print(printBuffer.append(resultArray[i][j]).append("\t").toString());
                printBuffer.delete(0, printBuffer.length());
            }
            System.out.println();
        }
        System.out.println();
    }
}