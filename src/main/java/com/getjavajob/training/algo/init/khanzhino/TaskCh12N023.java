package com.getjavajob.training.algo.init.khanzhino;

import java.util.Scanner;

public class TaskCh12N023 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int n;

        do {
            System.out.println("Type an odd number: ");
            n = sc.nextInt();
        } while (n % 2 == 0);

        printResultArray(getSolutionA(n));
        printResultArray(getSolutionB(n));
        printResultArray(getSolutionC(n));
    }

    public static int[][] getSolutionA(int n) {
        int[][] outputArray = new int[n][n];

        for (int i = 0; i < outputArray.length; i++) {
            for (int j = 0; j < outputArray.length; j++) {
                if (j == i || i + j == outputArray.length - 1) {
                    outputArray[i][j] = 1;
                } else  {
                    outputArray[i][j] = 0;
                }
            }
        }

        return outputArray;
    }

    public static int[][] getSolutionB(int n) {
        int[][] outputArray = new int[n][n];

        for (int i = 0; i < outputArray.length; i++) {
            for (int j = 0; j < outputArray.length; j++) {
                if (j == i || i + j == outputArray.length - 1 ||
                                  j == outputArray[0].length / 2 ||
                                  i == outputArray.length / 2) {
                    outputArray[i][j] = 1;
                } else {
                    outputArray[i][j] = 0;
                }
            }
        }

        return outputArray;
    }

    public static int[][] getSolutionC(int n) {
        int[][] outputArray = new int[n][n];
        int threshold = n / 2;

        for (int i = 0; i < outputArray.length; i++) {
            for (int j = 0; j <= threshold; j++) {
                int fillValue = 1;

                if (i < threshold) {
                    if (j < i) {
                        fillValue = 0;
                    }
                } else {
                    if (j < n - i - 1) {
                        fillValue = 0;
                    }
                }

                outputArray[i][j] = fillValue;
                outputArray[i][outputArray.length - j - 1] = fillValue;
            }
        }

        return outputArray;
    }

    public static void printResultArray(int[][] resultArray) {
        StringBuilder printBuffer = new StringBuilder();

        for (int i = 0; i < resultArray.length; i++) {
            for (int j = 0; j < resultArray[0].length; j++) {
                System.out.print(printBuffer.append(resultArray[i][j]).append("\t").toString());
                printBuffer.delete(0, printBuffer.length());
            }
            System.out.println();
        }
        System.out.println();
    }
}
