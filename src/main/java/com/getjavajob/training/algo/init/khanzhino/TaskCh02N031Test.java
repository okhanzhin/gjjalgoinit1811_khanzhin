package com.getjavajob.training.algo.init.khanzhino;

import static com.getjavajob.training.algo.init.khanzhino.TaskCh02N031.recombineNumber;
import static util.Assert.assertEquals;

public class TaskCh02N031Test {
    public static void main(String[] args) {
        testRecombineNumber();
        testBeyondTheLowerLimit();
        testAboveTheUpperLimit();
    }

    public static void testRecombineNumber() {
        assertEquals("TaskCh02N031Test", 213,
                                recombineNumber(123));
    }

    public static void testBeyondTheLowerLimit() {
        assertEquals("TaskCh02N031Test.testBeyondTheLowerLimit", -1, recombineNumber(99));
    }

    public static void testAboveTheUpperLimit() {
        assertEquals("TaskCh02N031Test.testAboveTheUpperLimit", -1, recombineNumber(1000));
    }
}