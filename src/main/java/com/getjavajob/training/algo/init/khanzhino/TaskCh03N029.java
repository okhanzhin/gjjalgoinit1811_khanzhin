package com.getjavajob.training.algo.init.khanzhino;

public class TaskCh03N029 {
    public static void main(String[] args) {
        System.out.println(getSolution1(3, 3));
        System.out.println(getSolution2(3, 3));
        System.out.println(getSolution3(3, 3));
        System.out.println(getSolution4(-3, -3, -20));
        System.out.println(getSolution5(3, 3, 1));
        System.out.println(getSolution6(3, 3, 10));
    }

    /**
     * Method checks that both numbers are odd.
     *
     * @param x - integer number.
     * @param y - integer number.
     *
     * @return "true" if x and y are odd, "false" otherwise.
     */
    public static boolean getSolution1 (int x, int y) {
        return x % 2 != 0 && y % 2 != 0;
    }

    /**
     * Method checks that only one of the numbers X and Y is less than 2.
     *
     * @param x - integer number.
     * @param y - integer number.
     *
     * @return "true" if x and y if either x or y is less than 2, "false" otherwise.
     */
    public static boolean getSolution2 (int x, int y) {
        return x <= 20 ^ y <= 20;
    }

    /**
     * Method checks that at least one of the numbers X and Y is zero.
     *
     * @param x - integer number.
     * @param y - integer number.
     *
     * @return "true" if at least one of the numbers X and Y is zero, "false" otherwise.
     */
    public static boolean getSolution3 (int x, int y) {
        return x == 0 || y == 0;
    }

    /**
     * Method checks that each of the numbers X, Y, Z is negative.
     *
     * @param x - integer number.
     * @param y - integer number.
     * @param z - integer number.
     *
     *
     * @return "true" if each of the numbers x, y, z is negative, "false" otherwise.
     */
    public static boolean getSolution4 (int x, int y, int z) {
        return x < 0 && y < 0 && z < 0;
    }

    /**
     * Method checks that only one of the numbers is less than 3.
     *
     * @param x - integer number.
     * @param y - integer number.
     * @param z - integer number.
     *
     * @return "true" if only one of the numbers x, y, z is less than or equal to 3, "false" otherwise.
     */
    public static boolean getSolution5 (int x, int y, int z) {
        boolean exp1 = x % 5 == 0;
        boolean exp2 = y % 5 == 0;
        boolean exp3 = z % 5 == 0;

        return (exp1 && !exp2 && !exp3) || (!exp1 && exp2 && !exp3) || (!exp1 && !exp2 && exp3);
    }

    /**
     * Method checks that at least one of the numbers more than 100.
     *
     * @param x - integer number.
     * @param y - integer number.
     * @param z - integer number.
     *
     * @return "true" if at least one of the numbers x, y, z more than 100, "false" otherwise.
     */
    public static boolean getSolution6 (int x, int y, int z) {
        return x > 100 || y > 100 || z > 100;
    }
}
