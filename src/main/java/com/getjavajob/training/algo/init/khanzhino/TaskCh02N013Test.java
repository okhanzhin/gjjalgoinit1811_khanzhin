package com.getjavajob.training.algo.init.khanzhino;

import static com.getjavajob.training.algo.init.khanzhino.TaskCh02N013.reverseNumber;
import static util.Assert.assertEquals;

public class TaskCh02N013Test {
    public static void main(String[] args) {
        testReverseFunction();
        testBeyondTheLowerLimit();
        testAboveTheUpperLimit();
    }

    public static void testReverseFunction() {
        assertEquals("TaskCh02N013Test.testReverseFunction", 321, reverseNumber(123));
    }

    public static void testBeyondTheLowerLimit() {
        assertEquals("TaskCh02N013Test.testBeyondTheLowerLimit", -1, reverseNumber(100));
    }

    public static void testAboveTheUpperLimit() {
        assertEquals("TaskCh02N013Test.testAboveTheUpperLimit", -1, reverseNumber(201));
    }
}
