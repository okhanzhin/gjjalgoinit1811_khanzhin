package com.getjavajob.training.algo.init.khanzhino;

import java.util.Scanner;

public class TaskCh09N022 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        String inputWord = "";

        do {
            System.out.println("Type a word with an even number of letters:");
            inputWord = sc.next();
        } while (inputWord.length() % 2 != 0);

        System.out.println(getHalfAWord(inputWord));
    }

    public static String getHalfAWord(String inputWord) {
        return inputWord.substring(0, inputWord.length() / 2);
    }
}
