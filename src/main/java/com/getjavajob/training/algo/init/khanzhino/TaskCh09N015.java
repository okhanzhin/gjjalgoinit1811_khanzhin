package com.getjavajob.training.algo.init.khanzhino;

import java.util.Scanner;

public class TaskCh09N015 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        String inputWord = sc.next();
        int numberOfCharacter = sc.nextInt();
        
        System.out.println(getDesiredCharacter(inputWord, numberOfCharacter));
    }

    public static char getDesiredCharacter(String inputWord, int numberOfCharacter) {
        return inputWord.charAt(numberOfCharacter - 1);
    }
}
