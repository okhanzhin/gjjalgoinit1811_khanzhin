package com.getjavajob.training.algo.init.khanzhino;

import static com.getjavajob.training.algo.init.khanzhino.TaskCh05N038.calculateDistanceFromHome;
import static com.getjavajob.training.algo.init.khanzhino.TaskCh05N038.calculateTraveledDistance;
import static util.Assert.assertEquals;

public class TaskCh05N038Test {
    public static void main(String[] args) {
        testDistanceFromHome();
        testTraveledDistance();
    }

    public static void testDistanceFromHome() {
        assertEquals("TaskCh05N038Test.testDistanceFromHome", 0.6456349206349207,
                                calculateDistanceFromHome(10));
    }

    public static void testTraveledDistance() {
        assertEquals("TaskCh05N038Test.testTraveledDistance", 2.9289682539682538,
                                calculateTraveledDistance(10));
    }
}
