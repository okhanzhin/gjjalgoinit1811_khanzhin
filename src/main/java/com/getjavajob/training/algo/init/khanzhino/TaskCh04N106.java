package com.getjavajob.training.algo.init.khanzhino;

import java.util.Scanner;

public class TaskCh04N106 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int actualMonth = sc.nextInt();

        System.out.println(determineSeason(actualMonth));
    }

    public static String determineSeason(int actualMonth) {
        switch (actualMonth) {
            case 12:
            case 1:
            case 2:
                return "winter";
            case 3:
            case 4:
            case 5:
                return "spring";
            case 6:
            case 7:
            case 8:
                return "summer";
            case 9:
            case 10:
            case 11:
                return "autumn";
            default:
                return "Enter the month number correctly!";
        }
    }
}
