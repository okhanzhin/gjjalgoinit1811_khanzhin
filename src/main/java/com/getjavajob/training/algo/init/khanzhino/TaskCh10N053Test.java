package com.getjavajob.training.algo.init.khanzhino;

import static com.getjavajob.training.algo.init.khanzhino.TaskCh10N053.startRecursion;
import static util.Assert.assertEquals;

public class TaskCh10N053Test {
    public static void main(String[] args) {
        testReverseArray();
    }

    public static void testReverseArray() {
        assertEquals("TaskCh10N053Test.testReverseArray", new int[]{3, 10, 7, 4, 1},
                                startRecursion(new int[]{1, 4, 7, 10, 3}));
    }
}
