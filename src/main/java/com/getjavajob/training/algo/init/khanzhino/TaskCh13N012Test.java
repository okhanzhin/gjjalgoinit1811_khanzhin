package com.getjavajob.training.algo.init.khanzhino;

import static com.getjavajob.training.algo.init.khanzhino.TaskCh13N012.*;
import static util.Assert.assertObject;
import static util.Assert.assertEquals;

import java.util.ArrayList;
import java.util.List;

public class TaskCh13N012Test {
    public static void main(String[] args) {
        testSearchingByDuration();
        testSearchingByNameMatch();
        testNumberOfWorkedYears();
    }

    public static void testSearchingByDuration() {
        initializeDatabase();

        List<Employee> employeesForDurationSearching = new ArrayList<>();
        employeesForDurationSearching.add(
        new Employee("Ivan", "Hlebnikov", "Moscow", 5, 2008));
        employeesForDurationSearching.add(
        new Employee("Vladimir", "Tkachenko", "Nizhniy Novgorod", 2, 2004));
        employeesForDurationSearching.add(
        new Employee("Mark", "Petrov", "Markovich","Moscow", 5, 2007));

        assertObject("TaskCh13N012Test.testSearchingByDuration",
        employeesForDurationSearching, searchByDuration(10, 2, 2019));
    }

    public static void testSearchingByNameMatch() {
        initializeDatabase();

        List<Employee> employeesForNameSearching = new ArrayList<>();
        employeesForNameSearching.add(
        new Employee("Ivan", "Hlebnikov", "Moscow", 5, 2008));
        employeesForNameSearching.add(
        new Employee("Nikolay",  "Shishkin" , "Ivanovich", "Saint-Petersburg", 12, 2010));
        employeesForNameSearching.add(
        new Employee("Ivan", "Samsonov", "Kaluga", 8, 2011));
        employeesForNameSearching.add(
        new Employee("Vladimir", "Ivanchuk", "Kostroma", 3, 2011));
        employeesForNameSearching.add(
        new Employee("Ivan", "Haritonov", "Moscow", 7, 2018));

        assertObject("TaskCh13N012Test.testSearchingByNameMatch",
        employeesForNameSearching, searchByNameMatch("ivan"));
    }

    public static void testNumberOfWorkedYears() {
        Employee testEmployee =
        new Employee("Oleg", "Losev", "Kaluga", 8, 2013);

        assertEquals("TaskCh13N012Test.testNumberOfWorkedYears", 5,
        testEmployee.getNumberOfWorkedYears(9, 2018));
        assertEquals("TaskCh13N012Test.testNumberOfWorkedYears", 4,
        testEmployee.getNumberOfWorkedYears(3, 2018));
    }
}
