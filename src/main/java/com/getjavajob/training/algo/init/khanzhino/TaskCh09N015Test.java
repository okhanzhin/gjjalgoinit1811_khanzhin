package com.getjavajob.training.algo.init.khanzhino;

import static com.getjavajob.training.algo.init.khanzhino.TaskCh09N015.getDesiredCharacter;
import static util.Assert.assertEquals;

public class TaskCh09N015Test {
    public static void main(String[] args) {
        testDesiredCharacter();
    }

    public static void testDesiredCharacter() {
        assertEquals("TaskCh09N015Test.testDesiredCharacter", 'e',
                      getDesiredCharacter("universe", 5));
    }
}
