package com.getjavajob.training.algo.init.khanzhino;

import static com.getjavajob.training.algo.init.khanzhino.TaskCh03N029.*;
import static util.Assert.assertBoolean;

public class TaskCh03N029Test {
    public static void main(String[] args) {
        testSolution1BothNumbersOdd();
        testSolution1FirstNumberOdd();
        testSolution1SecondNumberOdd();
        testSolution1BothNumbersEven();

        testSolution2BothNumbersLess20();
        testSolution2FirstNumberLess20();
        testSolution2SecondNumberLess20();
        testSolution2BothNumbersMore20();

        testSolution3BothNumbersNotZero();
        testSolution3FirstNumberIsZero();
        testSolution3SecondNumberIsZero();
        testSolution3BothNumbersZero();

        testSolution4AllNumbersNegative();
        testSolution4FirstNumbersNegativeOtherPositive();
        testSolution4SecondNumbersNegativeOtherPositive();
        testSolution4ThirdNumbersNegativeOtherPositive();
        testSolution4FirstAndSecondNegativeOtherPositive();
        testSolution4SecondAndThirdNegativeOtherPositive();
        testSolution4FirstAndThirdNegativeOtherPositive();
        testSolution4AllNumbersPositive();

        testSolution5AllNumbersMultiple5();
        testSolution5FirstNumberMultiple5();
        testSolution5SecondNumberMultiple5();
        testSolution5ThirdNumberMultiple5();
        testSolution5FirstAndSecondMultiple5();
        testSolution5SecondAndThirdMultiple5();
        testSolution5FirstAndThirdMultiple5();
        testSolution5AllNumbersNotMultiple5();

        testSolution6AllNumbersLessThan100();
        testSolution6FirstNumberMoreThan100();
        testSolution6SecondNumberMoreThan100();
        testSolution6ThirdNumberMoreThan100();
        testSolution6FirstAndSecondMoreThan100();
        testSolution6SecondAndThirdMoreThan100();
        testSolution6FirstAndThirdMoreThan100();
        testSolution6AllNumbersMoreThan100();
    }

    public static void testSolution1BothNumbersOdd() {
        assertBoolean("TaskCh03N029Test.testSolution1BothNumbersOdd", true,
                                getSolution1(5, 7));
    }

    public static void testSolution1FirstNumberOdd() {
        assertBoolean("TaskCh03N029Test.testSolution1FirstNumberOdd", false,
                                getSolution1(5, 8));
    }

    public static void testSolution1SecondNumberOdd() {
        assertBoolean("TaskCh03N029Test.testSolution1SecondNumberOdd", false,
                                getSolution1(8, 7));
    }

    public static void testSolution1BothNumbersEven() {
        assertBoolean("TaskCh03N029Test.testSolution1BothNumbersEven", false,
                                getSolution1(4, 6));
    }

    public static void testSolution2BothNumbersLess20() {
        assertBoolean("TaskCh03N029.testSolution2BothNumbersLess20", false,
                                getSolution2(1, 9));
    }

    public static void testSolution2FirstNumberLess20() {
        assertBoolean("TaskCh03N029.testSolution2FirstNumberLess20", true,
                                getSolution2(1, 25));
    }

    public static void testSolution2SecondNumberLess20() {
        assertBoolean("TaskCh03N029.testSolution2SecondNumberLess20", true,
                                getSolution2(25, 1));
    }

    public static void testSolution2BothNumbersMore20() {
        assertBoolean("TaskCh03N029.testSolution2BothNumbersMore20", false,
                                getSolution2(25, 40));
    }

    public static void testSolution3BothNumbersNotZero() {
        assertBoolean("TaskCh03N029.testSolution3BothNumbersNotZero", false,
                                getSolution3(10, 10));
    }

    public static void testSolution3FirstNumberIsZero() {
        assertBoolean("TaskCh03N029.testSolution3FirstNumberIsZero", true,
                                getSolution3(0, 10));
    }

    public static void testSolution3SecondNumberIsZero() {
        assertBoolean("TaskCh03N029.testSolution3SecondNumberIsZero", true,
                                getSolution3(10, 0));
    }

    public static void testSolution3BothNumbersZero() {
        assertBoolean("TaskCh03N029.testSolution3BothNumbersZero", true,
                                getSolution3(0, 0));
    }

    public static void testSolution4AllNumbersNegative() {
        assertBoolean("TaskCh03N029Test.testSolution4AllNumbersNegative", true,
                                getSolution4(-12, -7, -23));
    }

    public static void testSolution4FirstNumbersNegativeOtherPositive() {
        assertBoolean("TaskCh03N029Test.testSolution4FirstNumbersNegativeOtherPositive", false,
                                getSolution4(-12, 16, 0));
    }

    public static void testSolution4SecondNumbersNegativeOtherPositive() {
        assertBoolean("TaskCh03N029Test.testSolution4SecondNumbersNegativeOtherPositive", false,
                                getSolution4(10, -7, 0));
    }

    public static void testSolution4ThirdNumbersNegativeOtherPositive() {
        assertBoolean("TaskCh03N029Test.testSolution4ThirdNumbersNegativeOtherPositive", false,
                                getSolution4(11, 0, -23));
    }

    public static void testSolution4FirstAndSecondNegativeOtherPositive() {
        assertBoolean("TaskCh03N029Test.testSolution4FirstAndSecondNegativeOtherPositive", false,
                                getSolution4(-12, -7, 10));
    }

    public static void testSolution4SecondAndThirdNegativeOtherPositive() {
        assertBoolean("TaskCh03N029Test.testSolution4SecondAndThirdNegativeOtherPositive", false,
                                getSolution4(10, -7, -23));
    }

    public static void testSolution4FirstAndThirdNegativeOtherPositive() {
        assertBoolean("TaskCh03N029Test.testSolution4FirstAndThirdNegativeOtherPositive", false,
                                getSolution4(-12, 10, -23));
    }

    public static void testSolution4AllNumbersPositive() {
        assertBoolean("TaskCh03N029Test.testSolution4AllNumbersPositive", false,
                                getSolution4(13, 15, 17));
    }

    public static void testSolution5AllNumbersMultiple5() {
        assertBoolean("TaskCh03N029Test.testSolution5AllNumbersMultiple5", false,
                                getSolution5(5, 35, 50));
    }

    public static void testSolution5FirstNumberMultiple5() {
        assertBoolean("TaskCh03N029Test.testSolution5FirstNumberMultiple5", true,
                                getSolution5(5, 7, 2));
    }

    public static void testSolution5SecondNumberMultiple5() {
        assertBoolean("TaskCh03N029Test.testSolution5SecondNumberMultiple5", true,
                                getSolution5(2, 5, 7));
    }

    public static void testSolution5ThirdNumberMultiple5() {
        assertBoolean("TaskCh03N029Test.testSolution5ThirdNumberMultiple5", true,
                                getSolution5(7, 2, 5));
    }

    public static void testSolution5FirstAndSecondMultiple5() {
        assertBoolean("TaskCh03N029Test.testSolution5FirstAndSecondMultiple5", false,
                                getSolution5(5, 10, 2));
    }

    public static void testSolution5SecondAndThirdMultiple5() {
        assertBoolean("TaskCh03N029Test.testSolution5SecondAndThirdMultiple5", false,
                                getSolution5(2, 5, 10));
    }

    public static void testSolution5FirstAndThirdMultiple5() {
        assertBoolean("TaskCh03N029Test.testSolution5FirstAndThirdMultiple5", false,
                                getSolution5(5, 7, 10));
    }

    public static void testSolution5AllNumbersNotMultiple5() {
        assertBoolean("TaskCh03N029Test.testSolution5AllNumbersNotMultiple5", false,
                                getSolution5(3, 7, 8));
    }

    public static void testSolution6AllNumbersLessThan100() {
        assertBoolean("TaskCh03N029Test.testSolution6AllNumbersLessThan100", false,
                                getSolution6(50, 66, 70));
    }

    public static void testSolution6FirstNumberMoreThan100() {
        assertBoolean("TaskCh03N029Test.testSolution6FirstNumberMoreThan100", true,
                                getSolution6(110, 66, 70));
    }

    public static void testSolution6SecondNumberMoreThan100() {
        assertBoolean("TaskCh03N029Test.testSolution6SecondNumberMoreThan100", true,
                                getSolution6(50, 110, 70));
    }

    public static void testSolution6ThirdNumberMoreThan100() {
        assertBoolean("TaskCh03N029Test.testSolution6ThirdNumberMoreThan100", true,
                                getSolution6(50, 66, 105));
    }

    public static void testSolution6FirstAndSecondMoreThan100() {
        assertBoolean("TaskCh03N029Test.testSolution6FirstAndSecondMoreThan100", true,
                                getSolution6(110, 140, 10));
    }

    public static void testSolution6SecondAndThirdMoreThan100() {
        assertBoolean("TaskCh03N029Test.testSolution6SecondAndThirdMoreThan100", true,
                                getSolution6(50, 140, 105));
    }

    public static void testSolution6FirstAndThirdMoreThan100() {
        assertBoolean("TaskCh03N029Test.testSolution6FirstAndThirdMoreThan100", true,
                                getSolution6(110, 66, 105));
    }

    public static void testSolution6AllNumbersMoreThan100() {
        assertBoolean("TaskCh03N029Test.testSolution6AllNumbersMoreThan100", true,
                                getSolution6(150, 166, 105));
    }
}
