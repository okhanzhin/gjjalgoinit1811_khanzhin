package com.getjavajob.training.algo.init.khanzhino;

import java.util.Scanner;

public class TaskCh10N052 {
    private static final int TENS = 10;

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int requiredNumber = sc.nextInt();

        System.out.println(getReverseNumber(requiredNumber));
    }

    public static int getReverseNumber(int requiredNumber) {
        if (requiredNumber / TENS == 0) {
            return requiredNumber;
        }

        int num = getReverseNumber(requiredNumber / TENS);
        int remain = requiredNumber % TENS;
        int countOfDigits = 0;
        int bufferToSaveDischarge = requiredNumber;

        while (bufferToSaveDischarge / TENS != 0) {
            countOfDigits++;
            bufferToSaveDischarge /= TENS;
        }

        for (int i = 0; i < countOfDigits; i++) {
            remain *= TENS;
        }

        return remain + num;
    }
}
