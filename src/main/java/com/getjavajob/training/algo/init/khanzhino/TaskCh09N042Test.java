package com.getjavajob.training.algo.init.khanzhino;

import static com.getjavajob.training.algo.init.khanzhino.TaskCh09N042.reverseInputString;
import static util.Assert.assertStringBuilder;

public class TaskCh09N042Test {
    public static void main(String[] args) {
        testReverseInputString();
    }

    public static void testReverseInputString() {
        assertStringBuilder("TaskCh09N042Test.testReverseInputString", new StringBuilder("rebmun"),
                            reverseInputString("number"));
    }
}
