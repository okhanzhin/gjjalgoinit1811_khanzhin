package com.getjavajob.training.algo.init.khanzhino;

import java.util.Scanner;

public class TaskCh10N043 {
    private static final int TENS = 10;

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int inputNumber = sc.nextInt();

        System.out.println(sumDigitsOfNumber(inputNumber));
        System.out.println(getAmountOfDigits(inputNumber));
    }

    public static int sumDigitsOfNumber(int inputNumber) {
        if (inputNumber % TENS == inputNumber) {
            return inputNumber;
        } else {
            return (inputNumber % TENS + sumDigitsOfNumber(inputNumber / TENS));
        }
    }

    public static int getAmountOfDigits(int inputNumber) {
        if (inputNumber % TENS == inputNumber) {
            return 1;
        } else {
            return 1 + getAmountOfDigits(inputNumber / TENS);
        }
    }
}