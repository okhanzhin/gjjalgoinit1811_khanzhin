package com.getjavajob.training.algo.init.khanzhino;

import java.util.Scanner;

public class TaskCh10N050 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        System.out.println("Enter n parameter of Ackermann function: ");
        long n = sc.nextLong();
        System.out.println("Enter m parameter of Ackermann function: ");
        long m = sc.nextLong();

        System.out.println(calculateFunctionValue(n, m));
    }

    /**
     * Method calculates the value of the Ackermann function for two specified parameters
     *
     * @param n - parameter of Acсerman function.
     * @param m - parameter of Acсerman function.
     *
     * @return the result in a long type variable.
     */
    public static long calculateFunctionValue(long n, long m) {
        if (n == 0) {
            return m + 1;
        } else if (m == 0) {
            return calculateFunctionValue(n - 1, 1);
        } else {
            return calculateFunctionValue(n - 1, calculateFunctionValue(n, m - 1));
        }
    }
}
