package com.getjavajob.training.algo.init.khanzhino;

import static com.getjavajob.training.algo.init.khanzhino.TaskCh12N028.getSpiralArray;
import static util.Assert.assertEquals;

public class TaskCh12N028Test {
    public static void main(String[] args) {
        testSpiralFill();
    }

    public static void testSpiralFill() {
        assertEquals("TaskCh12N028Test.testSpiralFill", new int[][]{
                                {1, 2, 3, 4, 5},
                                {16, 17, 18, 19, 6},
                                {15, 24, 25, 20, 7},
                                {14, 23, 22, 21, 8},
                                {13, 12, 11, 10, 9}},
                                getSpiralArray(5));
    }
}
