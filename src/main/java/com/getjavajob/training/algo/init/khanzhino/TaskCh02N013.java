package com.getjavajob.training.algo.init.khanzhino;

import java.util.Scanner;

public class TaskCh02N013 {
    private static final int HUNDREDS = 100;
    private static final int TENS = 10;

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int source = sc.nextInt();

        System.out.println(reverseNumber(source));
    }

    public static int reverseNumber(int source) {
        if (source >= 101 && source < 200) {
            return HUNDREDS * (source % TENS) + TENS * (source / TENS % TENS) + source / HUNDREDS;
        } else {
            System.out.println("Please enter a number in the given interval.");
            return -1;
        }
    }
}