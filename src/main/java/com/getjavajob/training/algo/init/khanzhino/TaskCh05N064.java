package com.getjavajob.training.algo.init.khanzhino;

import java.util.Arrays;

public class  TaskCh05N064 {
    public static final int NUMBER_OF_AREAS = 12;

    public static void main(String[] args) {
        int[] areaSquare = {1000, 1100, 1200, 1300, 1400, 1500, 1600, 1700, 1800, 1900, 2000, 2100};
        int[] areaPopulation = {50000, 55000, 60000, 65000, 70000, 75000, 80000, 85000, 90000, 10000, 15000, 20000};

        System.out.println(findPopulationDensity(areaSquare, areaPopulation));
    }

    public static double findPopulationDensity(int[] areaSquare, int[] areaPopulation) {
        int totalAverage = 0;

        for (int i = 0; i <= areaPopulation.length - 1; i++) {
            totalAverage += areaSquare[i] * areaPopulation[i];
        }

        return (double)(totalAverage / areaPopulation.length);
    }
}
