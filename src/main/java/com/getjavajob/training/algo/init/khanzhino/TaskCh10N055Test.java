package com.getjavajob.training.algo.init.khanzhino;

import static com.getjavajob.training.algo.init.khanzhino.TaskCh10N055.getConversionResult;
import static util.Assert.assertObject;

public class TaskCh10N055Test {
    public static void main(String[] args) {
        testOctalConversion();
        testBinaryConversion();
        testHexadecimalConversion();
        testTwelveHexConversion();
        testBaseBeyondTheLowerLimit();
        testBaseAboveTheUpperLimit();
        testInputNumberOutOfRange();
    }

    public static void testOctalConversion() {
        assertObject("TaskCh10N055Test.testOctalConversion", "371",
                                getConversionResult(249, 8));
    }

    public static void testBinaryConversion() {
        assertObject("TaskCh10N055Test.testBinaryConversion", "11111001",
                                getConversionResult(249, 2));
    }

    public static void testHexadecimalConversion() {
        assertObject("TaskCh10N055Test.testHexadecimalConversion", "F9",
                                getConversionResult(249, 16));
    }

    public static void testTwelveHexConversion() {
        assertObject("TaskCh10N055Test.testTwelveHexConversion", "189",
                                getConversionResult(249, 12));
    }

    public static void testBaseBeyondTheLowerLimit() {
        assertObject("TaskCh10N055Test.testBaseBeyondTheLowerLimit", "Error: base out of bounds.",
                                getConversionResult(249, 0));
    }

    public static void testBaseAboveTheUpperLimit() {
        assertObject("TaskCh10N055Test.testBaseAboveTheUpperLimit", "Error: base out of bounds.",
                                getConversionResult(249, 20));
    }

    public static void testInputNumberOutOfRange() {
        assertObject("TaskCh10N055Test.testInputNumberOutOfRange",
                     "Error: input number out of range.",
                               getConversionResult(-1, 0));
    }
}
