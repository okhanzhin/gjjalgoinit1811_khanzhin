package com.getjavajob.training.algo.init.khanzhino;

import static com.getjavajob.training.algo.init.khanzhino.TaskCh10N050.calculateFunctionValue;
import static util.Assert.assertEquals;

public class TaskCh10N050Test {
    public static void main(String[] args) {
        testFunctionValue();
    }

    public static void testFunctionValue() {
        assertEquals("TaskCh10N050Test.testFunctionValue", 5, calculateFunctionValue(1, 3));
    }
}
