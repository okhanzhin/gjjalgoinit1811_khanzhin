package com.getjavajob.training.algo.init.khanzhino;

import static com.getjavajob.training.algo.init.khanzhino.TaskCh10N047.getFibonacciNumber;
import static util.Assert.assertEquals;

public class TaskCh10N047Test {
    public static void main(String[] args) {
        testFibonacciNumber();
    }

    public static void testFibonacciNumber() {
        assertEquals("TaskCh10N047Test.testFibonacciNumber", 13,
                                getFibonacciNumber(7));
    }
}
