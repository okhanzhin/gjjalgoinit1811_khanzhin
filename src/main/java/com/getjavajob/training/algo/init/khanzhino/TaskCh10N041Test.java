package com.getjavajob.training.algo.init.khanzhino;

import static com.getjavajob.training.algo.init.khanzhino.TaskCh10N041.calculateFactorial;
import static util.Assert.assertEquals;

public class TaskCh10N041Test {
    public static void main(String[] args) {
        testFactorialCalculation();
    }

    public static void testFactorialCalculation() {
        assertEquals("TaskCh10N041Test.testFactorialCalculation", 6,
                                calculateFactorial(3));
    }
}
