package com.getjavajob.training.algo.init.khanzhino;

import static com.getjavajob.training.algo.init.khanzhino.TaskCh05N064.findPopulationDensity;
import static util.Assert.assertEquals;

public class TaskCh05N064Test {
    public static void main(String[] args) {
        testPopulationDensityTable();
    }

    public static void testPopulationDensityTable() {
        assertEquals("TaskCh05N064Test.testPopulationDensityTable", 8.3583333E7,
        findPopulationDensity(new int[]{1000, 1100, 1200, 1300, 1400, 1500, 1600, 1700, 1800, 1900, 2000, 2100},
        new int[]{50000, 55000, 60000, 65000, 70000, 75000, 80000, 85000, 90000, 10000, 15000, 20000}));
    }
}
