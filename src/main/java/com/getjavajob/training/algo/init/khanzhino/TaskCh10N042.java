package com.getjavajob.training.algo.init.khanzhino;

import java.util.Scanner;

public class TaskCh10N042 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        double base = sc.nextDouble();
        int exponent = sc.nextInt();

        System.out.println(raiseNumberToPower(base, exponent));
    }

    public static double raiseNumberToPower(double base, int exponent) {
        if (exponent == 0) {
            return 1.0;
        } else if (exponent == 1) {
            return base;
        } else {
            return base * raiseNumberToPower(base, exponent - 1);
        }
    }
}
