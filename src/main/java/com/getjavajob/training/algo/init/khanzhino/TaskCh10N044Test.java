package com.getjavajob.training.algo.init.khanzhino;

import static com.getjavajob.training.algo.init.khanzhino.TaskCh10N044.getDigitalRoot;
import static com.getjavajob.training.algo.init.khanzhino.TaskCh10N044.getFinalRoot;
import static util.Assert.assertEquals;

public class TaskCh10N044Test {
    public static void main(String[] args) {
        testIntermediateDigitalRoot();
        testFinalDigitalRoot();
    }

    public static void testIntermediateDigitalRoot() {
        assertEquals("TaskCh10N044Test.testIntermediateDigitalRoot", 15,
                                getDigitalRoot(12345));
    }

    public static void testFinalDigitalRoot() {
        assertEquals("TaskCh10N044Test.testFinalDigitalRoot", 6,
                                getFinalRoot(12345));
    }
}
