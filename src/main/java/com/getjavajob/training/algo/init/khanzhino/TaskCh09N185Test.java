package com.getjavajob.training.algo.init.khanzhino;

import static com.getjavajob.training.algo.init.khanzhino.TaskCh09N185.checkCorrectPlaceOfParentheses;
import static com.getjavajob.training.algo.init.khanzhino.TaskCh09N185.checkCorrectPlaceOfParenthesesWithComment;
import static util.Assert.*;

public class TaskCh09N185Test {
    public static void main(String[] args) {
        testCorrectPlaceOfParentheses();
        testIncorrectPlaceOfParentheses();
        testIncorrectLeftParentheses();
        testIncorrectRightParentheses();
        testCorrectAllParentheses();
    }

    public static void testCorrectPlaceOfParentheses() {
        assertBoolean("TaskCh09N185Test.testCorrectPlaceOfParentheses", true,
                       checkCorrectPlaceOfParentheses("(4 + 5 * 2) − 5 / (7 − 2) / (2 + 1 + 12)"));
    }

    public static void testIncorrectPlaceOfParentheses() {
        assertBoolean("TaskCh09N185Test.testIncorrectPlaceOfParentheses", false,
                       checkCorrectPlaceOfParentheses("(4 + 5 * 2) − 5 / (7 − 2) / (2 + 1 + 12))"));
    }

    public static void testIncorrectLeftParentheses() {
        assertObject("TaskCh09N185Test.testIncorrectLeftParentheses",
                     "No. Number of extra opening brackets is 1.",
                    checkCorrectPlaceOfParenthesesWithComment("((4 + 5 * 2) − 5 / (7 − 2) / (2 + 1 + 12 )"));
    }

    public static void testIncorrectRightParentheses() {
        assertObject("TaskCh09N185Test.testIncorrectRightParentheses",
                     "No. Extra right bracket with index 42.",
                    checkCorrectPlaceOfParenthesesWithComment("(4 + 5 * 2) − 5 / (7 − 2) / (2 + 1 + 12 ))"));
    }

    public static void testCorrectAllParentheses() {
        assertObject("TaskCh09N185Test.testCorrectAllParentheses",
                     "All brackets are arranged correctly.",
                    checkCorrectPlaceOfParenthesesWithComment("(4 + 5 * 2) − 5 / (7 − 2) / (2 + 1 + 12 )"));
    }
}
