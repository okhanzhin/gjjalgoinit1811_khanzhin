package com.getjavajob.training.algo.init.khanzhino;

import java.util.Scanner;

public class TaskCh04N067 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int actualDay = sc.nextInt();

        System.out.println(checkWeekDay(actualDay));
    }

    public static String checkWeekDay(int actualDay) {
        String dayOfTheWeek = "";

        if (actualDay > 1 && actualDay <= 365) {
            if (actualDay % 7 >= 1 && actualDay % 7 <= 5) {
                dayOfTheWeek = "Workday";
            } else dayOfTheWeek = "Weekend";
        } else {
            System.out.println("Enter correct day of the year!");
        }

        return dayOfTheWeek;
    }
}
