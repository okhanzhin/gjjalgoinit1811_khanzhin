package com.getjavajob.training.algo.init.khanzhino;

import java.util.Scanner;

public class TaskCh10N046 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        double initialValue = sc.nextDouble();
        double stepOfProgression = sc.nextDouble();
        int numberRequiredMember = sc.nextInt();

        System.out.println(getGeometricMember(initialValue, stepOfProgression, numberRequiredMember));
        System.out.println(sumGeometricMembers(initialValue, stepOfProgression, numberRequiredMember));
    }

    public static double getGeometricMember(double initialValue, double stepOfProgression, int numberRequiredMember) {
        if (numberRequiredMember == 1) {
            return initialValue;
        } else {
            return stepOfProgression * getGeometricMember(initialValue, stepOfProgression,
                    numberRequiredMember - 1);
        }
    }

    public static double sumGeometricMembers(double initialValue, double stepOfProgression, int numberRequiredMember) {
        if (numberRequiredMember == 1) {
            return initialValue;
        } else {
            return initialValue + sumGeometricMembers(initialValue * stepOfProgression, stepOfProgression,
                    numberRequiredMember - 1);
        }
    }
}
