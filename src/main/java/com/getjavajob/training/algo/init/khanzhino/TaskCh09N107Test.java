package com.getjavajob.training.algo.init.khanzhino;

import static com.getjavajob.training.algo.init.khanzhino.TaskCh09N107.swapLetters;
import static util.Assert.assertObject;

public class TaskCh09N107Test {
    public static void main(String[] args) {
        testSwapLetters();
        testIncorrectWord();
    }

    public static void testSwapLetters() {
        assertObject("TaskCh09N107Test.testSwapLetters", "octar",
                                swapLetters("actor"));
    }

    public static void testIncorrectWord() {
        assertObject("TaskCh09N107Test.testIncorrectWord", "There are no right letters for swapping!",
                                swapLetters("number"));
    }
}
