package com.getjavajob.training.algo.init.khanzhino;

import java.util.Arrays;

public class TaskCh10N053 {
    public static void main(String[] args) {
        int[] requiredArray = {1, 4, 7, 10, 3};
        System.out.println(Arrays.toString(startRecursion(requiredArray)));
    }

    public static int[] startRecursion(int[] requiredNumbers) {
        reverseArray(requiredNumbers, 0, requiredNumbers.length - 1);
        return requiredNumbers;
    }

    public static void reverseArray(int[] requiredNumbers, int leftPosition, int rightPosition) {
        if (leftPosition < rightPosition) {
            int tmp = requiredNumbers[leftPosition];

            requiredNumbers[leftPosition] = requiredNumbers[rightPosition];
            requiredNumbers[rightPosition] = tmp;

            reverseArray(requiredNumbers, ++leftPosition, --rightPosition);
        }
    }
}
