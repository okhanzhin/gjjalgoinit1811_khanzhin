package com.getjavajob.training.algo.init.khanzhino;

import static com.getjavajob.training.algo.init.khanzhino.TaskCh02N039.getAngleBetweenArrows;
import static util.Assert.assertEquals;

public class TaskCh02N039Test {
    public static void main(String[] args) {
        testAngleBetweenArrows();
        testBeyondLowerHourLimit();
        testBeyondLowerMinuteLimit();
        testBeyondLowerSecondLimit();
        testAboveUpperHourLimit();
        testAboveUpperMinuteLimit();
        testAboveUpperSecondLimit();
    }

    public static void testAngleBetweenArrows() {
        assertEquals("TaskCh02N039Test.testAngleBetweenArrows", 90.0,
                                getAngleBetweenArrows(3, 0, 0));
    }

    public static void testBeyondLowerHourLimit() {
        assertEquals("TaskCh02N039Test.testBeyondLowerHourLimit", -1,
                                getAngleBetweenArrows(-1, 0, 0));
    }

    public static void testBeyondLowerMinuteLimit() {
        assertEquals("TaskCh02N039Test.testBeyondTheLowerMinuteLimit", -1,
                                getAngleBetweenArrows(0, -1, 0));
    }

    public static void testBeyondLowerSecondLimit() {
        assertEquals("TaskCh02N039Test.testBeyondLowerSecondLimit", -1,
                                getAngleBetweenArrows(0, 0, -1));
    }

    public static void testAboveUpperHourLimit() {
        assertEquals("TaskCh02N039Test.testAboveUpperHourLimit", -1,
                                getAngleBetweenArrows(24, 0, 0));
    }

    public static void testAboveUpperMinuteLimit() {
        assertEquals("TaskCh02N039Test.testAboveUpperMinuteLimit", -1,
                                getAngleBetweenArrows(0, 61, 0));
    }

    public static void testAboveUpperSecondLimit() {
        assertEquals("TaskCh02N039Test.testAboveUpperSecondLimit", -1,
                                getAngleBetweenArrows(0, 0, 61));
    }
}
