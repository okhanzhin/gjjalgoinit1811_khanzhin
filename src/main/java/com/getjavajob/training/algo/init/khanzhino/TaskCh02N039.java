package com.getjavajob.training.algo.init.khanzhino;

import java.util.Scanner;

public class TaskCh02N039 {
    private static final int HOURS_PER_HALFDAY = 12;
    private static final int DEGREES_PER_HOUR = 360 / HOURS_PER_HALFDAY;
    private static final int MINUTES_PER_HOUR = 60;
    private static final double DEGREES_PER_MINUTE = (double) DEGREES_PER_HOUR / MINUTES_PER_HOUR;
    private static final int SECONDS_PER_HOUR = 3600;
    private static final double DEGREES_PER_SECONDS = (double) DEGREES_PER_HOUR / SECONDS_PER_HOUR;

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int hours = sc.nextInt();
        int minutes = sc.nextInt();
        int seconds = sc.nextInt();

        System.out.println(getAngleBetweenArrows(hours, minutes, seconds));
    }

    /**
     * Method calculates the angle between the hour hand and its position at the beginning of the day.
     *
     * @param hours - current hours.
     * @param minutes - current minutes.
     * @param seconds - current seconds.
     *
     * @return real value of the angle in degrees.
     */
    public static double getAngleBetweenArrows(int hours, int minutes, int seconds) {
        if ((hours > 0 && hours <= 23) && (minutes >= 0 && minutes <= 59) && (seconds >= 0 && seconds <= 59)) {
            return hours * DEGREES_PER_HOUR + minutes * DEGREES_PER_MINUTE + seconds * DEGREES_PER_SECONDS;
        } else {
            return -1;
        }
    }
}
