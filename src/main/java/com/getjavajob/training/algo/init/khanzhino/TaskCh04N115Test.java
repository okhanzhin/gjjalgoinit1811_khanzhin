package com.getjavajob.training.algo.init.khanzhino;

import static com.getjavajob.training.algo.init.khanzhino.TaskCh04N115.getEasternCalendarNameSince1984;
import static com.getjavajob.training.algo.init.khanzhino.TaskCh04N115.getEasternCalendarNameWithinAD;
import static util.Assert.assertObject;

public class TaskCh04N115Test {
    public static void main(String[] args) {
        testYearSince1984();
        testIncorrectYearSince1984();
        testYearWithinAD();
        testIncorrectYearWithinAD();
    }

    public static void testYearSince1984() {
        assertObject("TaskCh04N115Test.testYearSince1984", "Green Monkey",
                                getEasternCalendarNameSince1984(2004));
    }

    public static void testIncorrectYearSince1984() {
        assertObject("TaskCh04N115Test.testIncorrectYearSince1984",
                     "Please enter year number since 1984 AD.",
                        getEasternCalendarNameSince1984(1980));
    }

    public static void testYearWithinAD() {
        assertObject("TaskCh04N115Test.testYearWithinAD",
                     "Black Сock",
                        getEasternCalendarNameWithinAD(13));
    }

    public static void testIncorrectYearWithinAD() {
        assertObject("TaskCh04N115Test.testIncorrectYearWithinAD",
                     "Please enter year natural number.",
                        getEasternCalendarNameWithinAD(0));
    }
}
