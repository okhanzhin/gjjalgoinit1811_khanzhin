package com.getjavajob.training.algo.init.khanzhino;

import java.util.Arrays;

public class TaskCh11N158 {
    public static void main(String[] args) {
        int[] inputArray = {1, 1, 3, 4, 5, 6, 6, 7, 7, 7, 8, 8, 10};

        System.out.println(Arrays.toString(inputArray));
        System.out.println(Arrays.toString(removeTheSameElement(inputArray)));
    }

    public static int[] removeTheSameElement(int[] inputArray) {
        int rightPointer = inputArray.length;
        int deletedCounter = 0;

        for (int i = 0; i < inputArray.length; i++) {
            boolean found = false;
            int current = i - deletedCounter;

            for (int j = 0; j < current; j++) {
                if (inputArray[current] == inputArray[j]) {
                    found = true;
                    break;
                }
            }

            if (found) {
                if (rightPointer - 1 - current >= 0)
                    System.arraycopy(inputArray, current + 1, inputArray, current,
                                                 rightPointer - 1 - current);

                inputArray[--rightPointer] = 0;
                deletedCounter++;
            }
        }

        return inputArray;
    }
}