package com.getjavajob.training.algo.init.khanzhino;

import static com.getjavajob.training.algo.init.khanzhino.TaskCh10N043.*;
import static util.Assert.*;

public class TaskCh10N043Test {
    public static void main(String[] args) {
        testSumOfDigits();
        testAmountOfDigits();
    }

    public static void testSumOfDigits() {
        assertEquals("TaskCh10N043Test.testSumOfDigits", 15, sumDigitsOfNumber(12345));
    }

    public static void testAmountOfDigits() {
        assertEquals("TaskCh10N043Test.testAmountOfDigits", 5, getAmountOfDigits(12345));
    }
}
