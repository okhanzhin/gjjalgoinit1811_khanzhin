package com.getjavajob.training.algo.init.khanzhino;

import java.util.Scanner;

public class TaskCh09N107 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        String inputString = sc.next();

        System.out.println(swapLetters(inputString));
    }

    public static String swapLetters(String inputString) {
        int indexOfFirstA = inputString.indexOf('a');
        int indexOfLastO = inputString.lastIndexOf('o');
        StringBuilder bufferForSwap = new StringBuilder(inputString);

        if (indexOfFirstA != -1 && indexOfLastO != -1) {
            bufferForSwap.setCharAt(indexOfFirstA, 'o');
            bufferForSwap.setCharAt(indexOfLastO, 'a');
        } else {
            bufferForSwap = new StringBuilder("There are no right letters for swapping!");
        }

        return bufferForSwap.toString();
    }
}
