package com.getjavajob.training.algo.init.khanzhino;

import java.util.Scanner;

public class TaskCh05N038 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int numberOfSegments = sc.nextInt();

        System.out.println("Distance from home equals " + calculateDistanceFromHome(numberOfSegments));
        System.out.println("The whole distance is " + calculateTraveledDistance(numberOfSegments));
    }

    public static double calculateDistanceFromHome(int numberOfSegments) {
        double distanceFromHome = 0.0;

        for (int i = 1; i <= numberOfSegments; ++i) {
            distanceFromHome += (2 * (i % 2.0) - 1.0) /  i;
        }

        return distanceFromHome;
    }

    public static double calculateTraveledDistance(int numberOfSegments) {
        double traveledDistance = 0.0;

        for (int i = 1; i <= numberOfSegments; ++i) {
            traveledDistance += 1.0 / i;
        }

        return traveledDistance;
    }
}
