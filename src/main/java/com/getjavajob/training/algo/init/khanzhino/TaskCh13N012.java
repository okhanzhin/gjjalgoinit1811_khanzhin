package com.getjavajob.training.algo.init.khanzhino;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class TaskCh13N012 {
    private static Database employeesDatabase;

    public static void main(String[] args) {
        initializeDatabase();

        List<Employee> employeesWithDuration = employeesDatabase.searchByDuration(10,
                                                                                2,
                                                                                 2019);

        for (Employee employee : employeesWithDuration) {
            System.out.println(employee);
        }

        System.out.println();

        List<Employee> employeesWithIvanName = employeesDatabase.searchByNameMatch("van");

        for (Employee employee : employeesWithIvanName) {
            System.out.println(employee);
        }
    }

    public static void initializeDatabase() {
        employeesDatabase = new Database();

        employeesDatabase.addEmployee(new Employee("Ivan", "Hlebnikov", "Moscow", 5, 2008));
        employeesDatabase.addEmployee(new Employee("Nikolay", "Shishkin", "Ivanovich", "Saint-Petersburg", 12, 2010));
        employeesDatabase.addEmployee(new Employee("Vladimir", "Tkachenko", "Nizhniy Novgorod", 2, 2004));
        employeesDatabase.addEmployee(new Employee("Fedor", "Sorokin", "Moscow", 7, 2012));
        employeesDatabase.addEmployee(new Employee("Pavel", "Popov", "Yaroslavl", 11, 2016));
        employeesDatabase.addEmployee(new Employee("Ivan", "Samsonov", "Kaluga", 8, 2011));
        employeesDatabase.addEmployee(new Employee("Victor", "Skrilev", "Moscow", 8, 2018));
        employeesDatabase.addEmployee(new Employee("Vladimir", "Zencov", "Saint-Petersburg", 8, 2017));
        employeesDatabase.addEmployee(new Employee("Sergey", "Kotov", "Nizhniy Novgorod", 8, 2011));
        employeesDatabase.addEmployee(new Employee("Oleg", "Losev", "Kaluga", 8, 2013));
        employeesDatabase.addEmployee(new Employee("Igor", "Mamleev", "Yaroslavl", 5, 2014));
        employeesDatabase.addEmployee(new Employee("Vladimir", "Ivanchuk", "Kostroma", 3, 2011));
        employeesDatabase.addEmployee(new Employee("Gagarin", "Yuri", "Baikonur", 12, 2010));
        employeesDatabase.addEmployee(new Employee("Mark", "Petrov", "Markovich","Moscow", 5, 2007));
        employeesDatabase.addEmployee(new Employee("Petr", "Yakunin", "Kaluga", 8, 2011));
        employeesDatabase.addEmployee(new Employee("Vyachislav", "Bolshev", "Kaluga", 8, 2014));
        employeesDatabase.addEmployee(new Employee("Ivan", "Haritonov", "Moscow", 7, 2018));
        employeesDatabase.addEmployee(new Employee("Kirill", "Mishin", "Kaluga", 8, 2013));
        employeesDatabase.addEmployee(new Employee("Nikolay", "Romanov", "Moscow", 5, 2010));
        employeesDatabase.addEmployee(new Employee("Petr", "Krivosheev", "Kaluga", 8, 2012));
    }

    public static List<Employee> searchByDuration(int threshold, int currentMonth, int currentYear) {
        return employeesDatabase.searchByDuration(threshold, currentMonth, currentYear);
    }

    public static List<Employee> searchByNameMatch(String nameTemp) {
        return employeesDatabase.searchByNameMatch(nameTemp);
    }
}

class Database {
    private List<Employee> employees;

    protected Database() {
        employees = new ArrayList<>();
    }

    public void addEmployee(Employee emp) {
        employees.add(emp);
    }

    public List<Employee> searchByDuration(int threshold, int currentMonth, int currentYear) {
        List<Employee> employeesByDuration = new ArrayList<>();

        for (Employee employee : employees) {
            if (employee.getNumberOfWorkedYears(currentMonth, currentYear) >= threshold) {
                employeesByDuration.add(employee);
            }
        }

        return employeesByDuration;
    }

    public List<Employee> searchByNameMatch(String nameTemp) {
        List<Employee> employeesByNameMatch = new ArrayList<>();
        nameTemp = nameTemp.toLowerCase();

        for (Employee employee : employees) {
            if ((employee.getName().toLowerCase().contains(nameTemp)) ||
                (employee.getPatronymic() != null &&
                 employee.getPatronymic().toLowerCase().contains(nameTemp)) ||
                (employee.getSurname().toLowerCase().contains(nameTemp))) {
                employeesByNameMatch.add(employee);
            }
        }

        return employeesByNameMatch;
    }
}

class Employee {
    private String name;
    private String patronymic;
    private String surname;
    private String address;
    private int monthOfEmployment;
    private int yearOfEmployment;

    public Employee(String name, String surname, String address, int monthOfEmployment, int yearOfEmployment) {
        this(name, surname, null, address, monthOfEmployment, yearOfEmployment);
    }

    public Employee(String name, String surname, String patronymic, String address,
                    int monthOfEmployment, int yearOfEmployment) {
        this.name = name;
        this.patronymic = patronymic;
        this.surname = surname;
        this.address = address;
        this.monthOfEmployment = monthOfEmployment;
        this.yearOfEmployment = yearOfEmployment;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setPatronymic(String patronymic) {
        this.patronymic = patronymic;
    }

    public String getPatronymic() {
        return patronymic;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getSurname() {
        return surname;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getAddress() {
        return address;
    }

    public int getMonthOfEmployment() {
        return monthOfEmployment;
    }

    public void setMonthOfEmployment(int monthOfEmployment) {
        this.monthOfEmployment = monthOfEmployment;
    }

    public int getYearOfEmployment() {
        return yearOfEmployment;
    }

    public void setYearOfEmployment(int yearOfEmployment) {
        this.yearOfEmployment = yearOfEmployment;
    }

    public int getNumberOfWorkedYears(int currentMonth, int currentYear) {
        int numberOfWorkedYears = currentYear - yearOfEmployment;
        if (monthOfEmployment > currentMonth) {
            numberOfWorkedYears = numberOfWorkedYears - 1;
        }

        return numberOfWorkedYears;
    }

    public String toString() {
        String res = "Name: " + name + " ";
        if (patronymic != null) {
            res += "Partonymic: " + patronymic + " ";
        }
        res += "Surname: " + surname + " \n";
        res += "Address: " + address + " ";
        res += "employmentMonth: " + monthOfEmployment + " ";
        res += "employmentYear: " + yearOfEmployment + " \n---------------------------";
        return res;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Employee employee = (Employee) o;
        return monthOfEmployment == employee.monthOfEmployment &&
                yearOfEmployment == employee.yearOfEmployment &&
                Objects.equals(name, employee.name) &&
                Objects.equals(patronymic, employee.patronymic) &&
                Objects.equals(surname, employee.surname) &&
                Objects.equals(address, employee.address);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, patronymic, surname, address, monthOfEmployment, yearOfEmployment);
    }
}
