package com.getjavajob.training.algo.init.khanzhino;

import static com.getjavajob.training.algo.init.khanzhino.TaskCh09N017.checkIdentityOfCharacters;
import static util.Assert.assertObject;

public class TaskCh09N017Test {
    public static void main(String[] args) {
        testIdentityOfCharactersAccept();
        testIdentityOfCharactersFailed();
    }

    public static void testIdentityOfCharactersAccept() {
        assertObject("TaskCh09N017Test.testIdentityOfCharactersAccept",
                      "Letters are identical.", checkIdentityOfCharacters("Level"));
    }

    public static void testIdentityOfCharactersFailed() {
        assertObject("TaskCh09N017Test.testIdentityOfCharactersFailed",
                      "Letters aren't the same.", checkIdentityOfCharacters("Some word"));
    }
}
