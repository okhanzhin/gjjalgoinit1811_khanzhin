package com.getjavajob.training.algo.init.khanzhino;

import static com.getjavajob.training.algo.init.khanzhino.TaskCh04N067.checkWeekDay;
import static util.Assert.assertObject;

public class TaskCh04N067Test {
    public static void main(String[] args) {
        testWorkday();
        testWeekend();
    }

    public static void testWorkday() {
        assertObject("TaskCh04N067Test.testWorkday", "Workday",
                                checkWeekDay(5));
    }

    public static void testWeekend() {
        assertObject("TaskCh04N067Test.testWeekend", "Weekend",
                                checkWeekDay(7));
    }
}
