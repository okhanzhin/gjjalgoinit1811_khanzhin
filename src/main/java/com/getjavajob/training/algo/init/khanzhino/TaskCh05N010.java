package com.getjavajob.training.algo.init.khanzhino;

import java.util.Scanner;

public class TaskCh05N010 {
    public static final int AMOUNT_OF_DOLLARS = 20;

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        double actualExchangeRate = sc.nextDouble();

        for (double tableValue : getExchangeTable(actualExchangeRate)) {
            System.out.println(tableValue);
        }
    }

    public static double[] getExchangeTable(double actualExchangeRate) {
        double[] completeExchangeTable = new double[AMOUNT_OF_DOLLARS];

        for (int i = 1; i <= AMOUNT_OF_DOLLARS; i++) {
            completeExchangeTable[i - 1] = actualExchangeRate * i;
        }

        return completeExchangeTable;
    }
}
