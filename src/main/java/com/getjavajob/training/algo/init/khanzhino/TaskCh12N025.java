package com.getjavajob.training.algo.init.khanzhino;

public class TaskCh12N025 {
    public static void main(String[] args) {
        printOutputArray(getSolution1());
        printOutputArray(getSolution2());
        printOutputArray(getSolution3());
        printOutputArray(getSolution4());
        printOutputArray(getSolution5());
        printOutputArray(getSolution6());
        printOutputArray(getSolution7());
        printOutputArray(getSolution8());
        printOutputArray(getSolution9());
        printOutputArray(getSolution10());
        printOutputArray(getSolution11());
        printOutputArray(getSolution12());
        printOutputArray(getSolution13());
        printOutputArray(getSolution14());
        printOutputArray(getSolution15());
        printOutputArray(getSolution16());
    }

    public static int[][] getSolution1() {
        int[][] outputArray = new int[12][10];
        int value = 0;

        for (int i = 0; i < outputArray.length; i++) {
            for (int j = 0; j < outputArray[i].length; j++) {
                outputArray[i][j] = ++value;
            }
        }

        return outputArray;
    }

    public static int[][] getSolution2() {
        int[][] outputArray = new int[12][10];
        int value = 0;

        for (int j = 0; j < outputArray[0].length; j++) {
            for (int i = 0; i < outputArray.length; i++) {
                outputArray[i][j] = ++value;
            }
        }

        return outputArray;
    }

    public static int[][] getSolution3() {
        int[][] outputArray = new int[12][10];
        int value = 0;

        for (int i = 0; i < outputArray.length; i++) {
            for (int j = outputArray[i].length - 1; j >= 0; j--) {
                outputArray[i][j] = ++value;
            }
        }

        return outputArray;
    }

    public static int[][] getSolution4() {
        int[][] outputArray = new int[12][10];
        int value = 0;

        for (int j = 0; j < outputArray[0].length; j++) {
            for (int i = outputArray.length - 1; i >= 0; i--) {
                outputArray[i][j] = ++value;
            }
        }

        return outputArray;
    }

    public static int[][] getSolution5() {
        int[][] outputArray = new int[10][12];
        int value = 0;

        for (int i = 0; i < outputArray.length; i++) {
            if (i % 2 == 0) {
                for (int j = 0; j < outputArray[0].length; j++) {
                    outputArray[i][j] = ++value;
                }
            } else {
                for (int j = outputArray[0].length - 1; j >= 0; j--) {
                    outputArray[i][j] = ++value;
                }
            }
        }

        return outputArray;
    }

    public static int[][] getSolution6() {
        int[][] outputArray = new int[12][10];
        int value = 0;

        for (int j = 0; j < outputArray[0].length; j++) {
            if (j % 2 == 0) {
                for (int i = 0; i < outputArray.length; i++) {
                    outputArray[i][j] = ++value;
                }
            } else {
                for (int i = outputArray.length - 1; i >= 0; i--) {
                    outputArray[i][j] = ++value;
                }
            }
        }

        return outputArray;
    }

    public static int[][] getSolution7() {
        int[][] outputArray = new int[12][10];
        int value = 0;

        for (int i = outputArray.length - 1; i >= 0; i--) {
            for (int j = 0; j < outputArray[0].length; j++) {
                outputArray[i][j] = ++value;
            }
        }

        return outputArray;
    }

    public static int[][] getSolution8() {
        int[][] outputArray = new int[12][10];
        int value = 0;

        for (int j = outputArray[0].length - 1; j >= 0; j--) {
            for (int i = 0; i <= outputArray.length - 1; i++) {
                outputArray[i][j] = ++value;
            }
        }

        return outputArray;
    }

    public static int[][] getSolution9() {
        int[][] outputArray = new int[12][10];
        int value = 0;

        for (int i = outputArray.length - 1; i >= 0; i--) {
            for (int j = outputArray[0].length - 1; j >= 0; j--) {
                outputArray[i][j] = ++value;
            }
        }

        return outputArray;
    }

    public static int[][] getSolution10() {
        int[][] outputArray = new int[12][10];
        int value = 0;

        for (int j = outputArray[0].length - 1; j >= 0; j--) {
            for (int i = outputArray.length - 1; i >= 0; i--) {
                outputArray[i][j] = ++value;
            }
        }

        return outputArray;
    }

    public static int[][] getSolution11() {
        int[][] outputArray = new int[12][10];
        int value = 0;

        for (int i = outputArray.length - 1; i >= 0; i--) {
            if (i % 2 != 0) {
                for (int j = 0; j < outputArray[0].length; j++) {
                    outputArray[i][j] = ++value;
                }
            } else {
                for (int j = outputArray[0].length - 1; j >= 0; j--) {
                    outputArray[i][j] = ++value;
                }
            }
        }

        return outputArray;
    }

    public static int[][] getSolution12() {
        int[][] outputArray = new int[12][10];
        int value = 0;

        for (int i = 0; i < outputArray.length; i++) {
            if (i % 2 == 0) {
                for (int j = outputArray[0].length - 1; j >= 0; j--) {
                    outputArray[i][j] = ++value;
                }
            } else {
                for (int j = 0; j < outputArray[0].length; j++) {
                    outputArray[i][j] = ++value;
                }
            }
        }


        return outputArray;
    }

    public static int[][] getSolution13() {
        int[][] outputArray = new int[12][10];
        int value = 0;

        for (int j = outputArray[0].length - 1; j >= 0; j--) {
            if (j % 2 != 0) {
                for (int i = 0; i < outputArray.length; i++) {
                    outputArray[i][j] = ++value;
                }
            } else {
                for (int i = outputArray.length - 1; i >= 0; i--) {
                    outputArray[i][j] = ++value;
                }
            }
        }

        return outputArray;
    }

    public static int[][] getSolution14() {
        int[][] outputArray = new int[12][10];
        int value = 0;

        for (int j = 0; j < outputArray[0].length; j++) {
            if (j % 2 == 0) {
                for (int i = outputArray.length - 1; i >= 0; i--) {
                    outputArray[i][j] = ++value;
                }
            } else {
                for (int i = 0; i < outputArray.length; i++) {
                    outputArray[i][j] = ++value;
                }
            }
        }

        return outputArray;
    }

    public static int[][] getSolution15() {
        int[][] outputArray = new int[12][10];
        int value = 0;

        for (int i = outputArray.length - 1; i >= 0; i--) {
            if (i % 2 != 0) {
                for (int j = outputArray[0].length - 1; j >= 0; j--) {
                    outputArray[i][j] = ++value;
                }
            } else {
                for (int j = 0; j < outputArray[0].length; j++) {
                    outputArray[i][j] = ++value;
                }
            }
        }

        return outputArray;
    }

    public static int[][] getSolution16() {
        int[][] outputArray = new int[12][10];
        int value = 0;

        for (int j = outputArray[0].length - 1; j >= 0; j--) {
            if (j % 2 != 0) {
                for (int i = outputArray.length - 1; i >= 0; i--) {
                    outputArray[i][j] = ++value;
                }
            } else {
                for (int i = 0; i < outputArray.length; i++) {
                    outputArray[i][j] = ++value;
                }
            }
        }

        return outputArray;
    }


    public static void printOutputArray(int[][] inputArray) {
        StringBuilder printBuffer = new StringBuilder();

        for (int i = 0; i < inputArray.length; i++) {
            for (int j = 0; j < inputArray[0].length; j++) {
                System.out.print(printBuffer.append(inputArray[i][j]).append("\t").toString());
                printBuffer.delete(0, printBuffer.length());
            }
            System.out.println();
        }
        System.out.println();
    }
}
