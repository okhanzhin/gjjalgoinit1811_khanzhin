package com.getjavajob.training.algo.init.khanzhino;

import java.util.Scanner;

public class TaskCh02N031 {
    private static final int HUNDREDS = 100;
    private static final int TENS = 10;

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int source = sc.nextInt();

        System.out.println(recombineNumber(source));
    }

    public static int recombineNumber(int source) {
        if (source >= 100 && source <= 999) {
            return HUNDREDS * (source / TENS % TENS) + TENS * (source / HUNDREDS) + (source % TENS);
        } else {
            System.out.println("Please enter a three-digit number.");
            return -1;
        }
    }
}
