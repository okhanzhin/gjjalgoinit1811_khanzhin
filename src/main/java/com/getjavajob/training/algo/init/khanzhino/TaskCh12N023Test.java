package com.getjavajob.training.algo.init.khanzhino;

import static com.getjavajob.training.algo.init.khanzhino.TaskCh12N023.getSolutionA;
import static com.getjavajob.training.algo.init.khanzhino.TaskCh12N023.getSolutionB;
import static com.getjavajob.training.algo.init.khanzhino.TaskCh12N023.getSolutionC;
import static util.Assert.assertEquals;

public class TaskCh12N023Test {
    public static void main(String[] args) {
        testSolutionA();
        testSolutionB();
        testSolutionC();
    }

    public static void testSolutionA() {
        assertEquals("TaskCh12N023Test.testSolutionA", new int[][]{
                        {1, 0, 0, 0, 0, 0, 1},
                        {0, 1, 0, 0, 0, 1, 0},
                        {0, 0, 1, 0, 1, 0, 0},
                        {0, 0, 0, 1, 0, 0, 0},
                        {0, 0, 1, 0, 1, 0, 0},
                        {0, 1, 0, 0, 0, 1, 0},
                        {1, 0, 0, 0, 0, 0, 1}},
                        getSolutionA(7));
    }

    public static void testSolutionB() {
        assertEquals("TaskCh12N023Test.testSolutionB", new int[][]{
                        {1, 0, 0, 1, 0, 0, 1},
                        {0, 1, 0, 1, 0, 1, 0},
                        {0, 0, 1, 1, 1, 0, 0},
                        {1, 1, 1, 1, 1, 1, 1},
                        {0, 0, 1, 1, 1, 0, 0},
                        {0, 1, 0, 1, 0, 1, 0},
                        {1, 0, 0, 1, 0, 0, 1}},
                        getSolutionB(7));
    }

    public static void testSolutionC() {
        assertEquals("TaskCh12N023Test.testSolutionC", new int[][]{
                        {1, 1, 1, 1, 1, 1, 1},
                        {0, 1, 1, 1, 1, 1, 0},
                        {0, 0, 1, 1, 1, 0, 0},
                        {0, 0, 0, 1, 0, 0, 0},
                        {0, 0, 1, 1, 1, 0, 0},
                        {0, 1, 1, 1, 1, 1, 0},
                        {1, 1, 1, 1, 1, 1, 1}},
                        getSolutionC(7));
    }
}