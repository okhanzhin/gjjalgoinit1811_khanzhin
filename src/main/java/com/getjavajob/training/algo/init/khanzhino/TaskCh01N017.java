package com.getjavajob.training.algo.init.khanzhino;

import static java.lang.Math.*;

public class TaskCh01N017 {
   public static double calculateOperation1 (int x) {
      return sqrt(1 - pow(sin(x), 2));
   }

   public static double calculateOperation2 (int x, int a, int b, int c) {
      return 1 / (sqrt(a * pow(x, 2) + b * x + c));
   }

   public static double calculateOperation3 (int x) {
      return (sqrt(1 + x) + sqrt(1 - x)) / (2 * sqrt(x));
   }

   public static double calculateOperation4 (int x) {
      return abs(x) + abs(x + 1);
   }
}
