package com.getjavajob.training.algo.init.khanzhino;

import java.util.Scanner;

public class TaskCh04N036 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        double t = sc.nextDouble();

        System.out.println(getTrafficLightColor(t));
    }

    public static String getTrafficLightColor(double t) {
        double intervalTime = t % 5.0;

        return intervalTime >= 0.0 && intervalTime < 3.0 ? "green" : "red";
    }
}
