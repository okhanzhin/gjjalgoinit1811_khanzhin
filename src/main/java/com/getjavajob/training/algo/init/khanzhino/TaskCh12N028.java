package com.getjavajob.training.algo.init.khanzhino;

import java.util.Scanner;

import static java.lang.Math.pow;

public class TaskCh12N028 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int n;

        do {
            System.out.println("Type a word with an even number of letters:");
            n = sc.nextInt();
        } while (n % 2 != 1);

        printOutputArray(getSpiralArray(n));
    }

    public static int[][] getSpiralArray(int n) {
        int[][] outputArray = new int[n][n];
        boolean[][] visit = new boolean[n][n];
        outputArray[0][0] = 1;
        visit[0][0] = true;

        int value = 1;
        int direction = 1;
        int i = 0;
        int j = 0;
        int num = 1;

        while (true) {
            switch (direction) {
                case 1:
                    if (j + 1 < n && !visit[i][j + 1]) {
                        j++;
                        visit[i][j] = true;
                        outputArray[i][j] = ++value;
                        num++;
                    } else {
                        direction++;
                    }
                    break;
                case 2:
                    if (i + 1 < n && !visit[i + 1][j]) {
                        i++;
                        visit[i][j] = true;
                        outputArray[i][j] = ++value;
                        num++;
                    } else {
                        direction++;
                    }
                    break;
                case 3:
                    if (j - 1 >= 0 && !visit[i][j - 1]) {
                        j--;
                        visit[i][j] = true;
                        outputArray[i][j] = ++value;
                        num++;
                    } else {
                        direction++;
                    }
                    break;
                case 4:
                    if (i - 1 >= 0 && !visit[i - 1][j]) {
                        i--;
                        visit[i][j] = true;
                        outputArray[i][j] = ++value;
                        num++;
                    } else {
                        direction = 1;
                    }
                    break;
                default:
                    System.out.println("ERROR");
            }

            if (num == pow(n, 2)) {
                return outputArray;
            }
        }
    }

    public static void printOutputArray(int[][] inputArray) {
        StringBuilder printBuffer = new StringBuilder();

        for (int i = 0; i < inputArray.length; i++) {
            for (int j = 0; j < inputArray[0].length; j++) {
                System.out.print(printBuffer.append(inputArray[i][j]).append("\t").toString());
                printBuffer.delete(0, printBuffer.length());
            }
            System.out.println();
        }
        System.out.println();
    }
}