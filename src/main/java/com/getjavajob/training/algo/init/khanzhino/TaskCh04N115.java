package com.getjavajob.training.algo.init.khanzhino;

import java.util.Scanner;

public class TaskCh04N115 {
    private static final int ANIMALS_PER_CYCLE = 12;
    private static final int COLORS_PER_CYCLE = 10;
    private static final int YEARS_PER_CYCLE = 60;
    private static final int START_YEAR = 1984;
    private static final int ANIMALS_PER_COLOR_CASE = 2;


    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int inputYear = sc.nextInt();

        System.out.println(getEasternCalendarNameSince1984(inputYear));
        System.out.println(getEasternCalendarNameWithinAD(inputYear));
    }

    public static String getEasternCalendarNameSince1984(int inputYear) {
        if (inputYear >= START_YEAR) {
            String[] color = {"Green", "Red", "Yellow", "White", "Black"};
            String[] animals =
            {"Rat", "Cow", "Tiger", "Rabbit", "Dragon", "Snake", "Horse", "Sheep", "Monkey", "Сock", "Dog", "Pig"};

            return color[((inputYear - START_YEAR) % COLORS_PER_CYCLE) / ANIMALS_PER_COLOR_CASE] + " " +
                    animals[(inputYear - START_YEAR) % ANIMALS_PER_CYCLE];
        } else {
            return "Please enter year number since 1984 AD.";
        }
    }

    public static String getEasternCalendarNameWithinAD(int inputYear) {
        int year = START_YEAR % YEARS_PER_CYCLE - YEARS_PER_CYCLE;

        if (inputYear >= 1) {
            String[] color = {"Green", "Red", "Yellow", "White", "Black"};
            String[] animals =
            {"Rat", "Cow", "Tiger", "Rabbit", "Dragon", "Snake", "Horse", "Sheep", "Monkey", "Сock", "Dog", "Pig"};

            return color[((inputYear - year) % COLORS_PER_CYCLE) / ANIMALS_PER_COLOR_CASE] + " " +
                    animals[(inputYear - year) % ANIMALS_PER_CYCLE];
        } else {
            return "Please enter year natural number.";
        }
    }
}
