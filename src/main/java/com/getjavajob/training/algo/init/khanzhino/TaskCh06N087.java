package com.getjavajob.training.algo.init.khanzhino;

import java.util.Scanner;

public class TaskCh06N087 {
    public static void main(String[] args) {
        Game game = new Game();
        game.play();
    }
}

class Game {
    private String team1;
    private String team2;
    private int score1;
    private int score2;

    /**
     * here will be implemented console input logic for 2 players
     */
    void play() {
        Scanner scanner = new Scanner(System.in);

        System.out.println("Enter team #1:");
        team1 = scanner.next();
        System.out.println("Enter team #2:");
        team2 = scanner.next();

        System.out.println("The match between " + team1 + " : " + team2 + " has begun!");
        boolean game = true;

        while (game) {
            System.out.println("Enter team to score (1 or 2 or 0 to finish game):");
            int input = scanner.nextInt();

            switch (input) {
                case 0: {
                    game = false;
                    break;
                }
                case 1: {
                    System.out.println("Enter score (1 or 2 or 3):");
                    int points = scanner.nextInt();
                    score1 += points;
                    System.out.println(score());
                    break;
                }
                case 2: {
                    System.out.println("Enter score (1 or 2 or 3):");
                    int points = scanner.nextInt();
                    score2 += points;
                    System.out.println(score());
                    break;
                }
                default: {
                    System.out.println("Invalid team!");
                }
            }
        }

        System.out.println(result());
    }

    /**
     * intermediate score
     */
    String score() {
        return team1 + " " + score1 + " : " + score2 + " " + team2;
    }

    /**
     * result with winner, loser and their scores
     */
    String result() {
        if (score1 < score2) {
            return "WINNER " + team2 + " " + score2 + " - " + score1 + " " + team1;
        } else if (score2 < score1) {
            return "WINNER " + team1 + " " + score1 + " - " + score2 + " " + team2;
        } else {
            return "DRAW" + team1 + " " + score1 + " - " + score2 + " " + team2;
        }
    }

    public String getTeam1() {
        return team1;
    }

    public void setTeam1(String team1) {
        this.team1 = team1;
    }

    public String getTeam2() {
        return team2;
    }

    public void setTeam2(String team2) {
        this.team2 = team2;
    }

    public int getScore1() {
        return score1;
    }

    public void setScore1(int score1) {
        this.score1 = score1;
    }

    public int getScore2() {
        return score2;
    }

    public void setScore2(int score2) {
        this.score2 = score2;
    }
}
