package com.getjavajob.training.algo.init.khanzhino;

import static com.getjavajob.training.algo.init.khanzhino.TaskCh12N063.getAverageInParallel;
import static util.Assert.assertEquals;

public class TaskCh12N063Test {
    public static void main(String[] args) {
        testAverageInParallel();
    }

    public static void testAverageInParallel() {
        assertEquals("TaskCh12N063Test.testAverageInParallel",
                                new int[]{25, 19, 19, 19, 19, 19, 11, 19, 19, 19, 19},
                                getAverageInParallel(new int[][]{{19, 50, 16, 18},
                                                                 {23, 20, 19, 15},
                                                                 {23, 20, 19, 15},
                                                                 {23, 20, 19, 15},
                                                                 {23, 20, 19, 15},
                                                                 {23, 20, 19, 15},
                                                                 {5, 10, 15, 17},
                                                                 {23, 20, 19, 15},
                                                                 {23, 20, 19, 15},
                                                                 {23, 20, 19, 15},
                                                                 {23, 20, 19, 15}}));
    }
}
