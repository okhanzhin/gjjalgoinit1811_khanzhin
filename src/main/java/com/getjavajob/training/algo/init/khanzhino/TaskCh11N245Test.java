package com.getjavajob.training.algo.init.khanzhino;

import static com.getjavajob.training.algo.init.khanzhino.TaskCh11N245.getNegativeFirst;
import static util.Assert.assertEquals;

public class TaskCh11N245Test {
    public static void main(String[] args) {
        testGettingNegativeFirst();
    }

    public static void testGettingNegativeFirst() {
        assertEquals("TaskCh11N245Test.testGettingNegativeFirst",
                                new int[]{-1, -2, -7, -23, 7, 5, 4, 0, 12, 15, 10},
                                getNegativeFirst(new int[]{7, 5, 4, -1, -2, 0, 12, 15, 10, -7, -23}));
    }
}
