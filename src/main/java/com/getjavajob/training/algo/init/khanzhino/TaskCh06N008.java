package com.getjavajob.training.algo.init.khanzhino;

import java.util.Arrays;
import java.util.Scanner;

public class TaskCh06N008 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int limitNumber = sc.nextInt();

        System.out.println(Arrays.toString(calculateSeriesOfPossibleNumbers(limitNumber)));
    }

    /**
     * Method returns numbers from a series of squares of natural numbers that do not exceed the specified threshold value.
     *
     * @param limitNumber - specified threshold value.
     * @return Array of integer numbers.
     */
    public static int[] calculateSeriesOfPossibleNumbers(int limitNumber) {
        int[] primaryPossibleNumbers = new int[limitNumber];
        int countOfNumbers = 0;

        for (int i = 1; i * i <= primaryPossibleNumbers.length; i++) {
            primaryPossibleNumbers[i - 1] = i * i;
            countOfNumbers++;
        }

        int[] possibleNumbers = new int[countOfNumbers];

        for (int i = 0; i < possibleNumbers.length; i++) {
            possibleNumbers[i] = primaryPossibleNumbers[i];
        }

        return possibleNumbers;
    }
}
