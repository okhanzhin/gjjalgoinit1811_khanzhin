package com.getjavajob.training.algo.init.khanzhino;

import static com.getjavajob.training.algo.init.khanzhino.TaskCh10N049.getIndexOfMaxElement;
import static util.Assert.assertEquals;

public class TaskCh10N049Test {
    public static void main(String[] args) {
        testIndexOfMaxElement();
    }

    public static void testIndexOfMaxElement() {
        assertEquals("TaskCh10N049Test.testIndexOfMaxElement", 2,
                                getIndexOfMaxElement(new int[]{1, 2, 7, 2, 6, 4, 6, 1, 2, 6}, 0));
    }
}
