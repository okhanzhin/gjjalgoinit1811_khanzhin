package com.getjavajob.training.algo.init.khanzhino;

import static com.getjavajob.training.algo.init.khanzhino.TaskCh04N036.getTrafficLightColor;
import static util.Assert.assertObject;

public class TaskCh04N036Test {
    public static void main(String[] args) {
        testRedLight();
        testGreenLight();
    }

    public static void testRedLight() {
        assertObject("TaskCh04N036Test.testRedLight", "red",
                                getTrafficLightColor(3));
    }

    public static void testGreenLight() {
        assertObject("TaskCh04N036Test.testGreenLight", "green",
                                getTrafficLightColor(5));
    }
}
