package com.getjavajob.training.algo.init.khanzhino;

import java.io.BufferedWriter;
import java.io.File;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;

import static java.nio.file.Files.createFile;
import static java.nio.file.Files.delete;

public class TaskCh10N051 {
    static Path p = Paths.get("/Volumes/Library/dev/projects/getjavajob/algo-init/src/main/java/com/getjavajob/training/algo/init/khanzhino/TaskCh10N051.ActualOutputs.txt");

    public static void main(String[] args) {
        try {
            if (Files.notExists(p)) {
                createFile(p);
                System.out.println("file created");
            } else {
                delete(p);
                createFile(p);
            }

            BufferedWriter bufferedWriter = Files.newBufferedWriter(p, StandardOpenOption.APPEND);
            bufferedWriter.write("alg1: ");
            bufferedWriter.flush();

            runAlgorithm1(5, bufferedWriter);

            bufferedWriter.newLine();
            bufferedWriter.flush();
            bufferedWriter.close();

            BufferedWriter bufferedWriter2 = Files.newBufferedWriter(p, StandardOpenOption.APPEND);
            bufferedWriter2.write("alg2: ");
            bufferedWriter2.flush();

            runAlgorithm2(5, bufferedWriter2);

            bufferedWriter2.newLine();
            bufferedWriter2.flush();
            bufferedWriter2.close();

            BufferedWriter bufferedWriter3 = Files.newBufferedWriter(p, StandardOpenOption.APPEND);
            bufferedWriter3.write("alg3: ");
            bufferedWriter3.flush();

            runAlgorithm3(5, bufferedWriter3);

            bufferedWriter3.newLine();
            bufferedWriter3.flush();
            bufferedWriter3.close();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void runAlgorithm1(int x, BufferedWriter bufferedWriter) {
        try {
            if (x > 0) {
                bufferedWriter.write(x + " ");
                bufferedWriter.flush();
                runAlgorithm1(x - 1, bufferedWriter);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void runAlgorithm2(int x, BufferedWriter bufferedWriter2) {
        try {
            if (x > 0) {
                runAlgorithm2(x - 1, bufferedWriter2);
                bufferedWriter2.write(x + " ");
                bufferedWriter2.flush();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void runAlgorithm3(int x, BufferedWriter bufferedWriter3) {
        try {
            if (x > 0) {
                bufferedWriter3.write(x + " ");
                bufferedWriter3.flush();
                runAlgorithm3(x - 1, bufferedWriter3);
                bufferedWriter3.write(x + " ");
                bufferedWriter3.flush();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
