package com.getjavajob.training.algo.init.khanzhino;

import static com.getjavajob.training.algo.init.khanzhino.TaskCh10N048.getMaxElementOfArray;
import static com.getjavajob.training.algo.init.khanzhino.TaskCh10N048.chooseMax;
import static util.Assert.assertEquals;

public class TaskCh10N048Test {
    public static void main(String[] args) {
        testMaxElementOfArray();
        testIntermediateChooseMethod();
    }

    public static void testMaxElementOfArray() {
        assertEquals("TaskCh10N048Test.testMaxElementOfArray.testMaxElementOfArray", 7,
                getMaxElementOfArray(new int[]{1, 2, 7, 2, 6, 4, 6, 1, 2, 6}, 10));
    }

    public static void testIntermediateChooseMethod() {
        assertEquals("TaskCh10N048Test.testIntermediateChooseMethod", 10, chooseMax(5, 10));
    }
}
