package com.getjavajob.training.algo.init.khanzhino;

public class TaskCh09N166 {
    public static void main(String[] args) {
        System.out.println(swapFirstAndLastWord("Today is a great day, by the way?"));
    }

    public static String swapFirstAndLastWord(String sourcePhrase) {
        String[] wordsInPhrase = sourcePhrase.split(" ");

        String bufferLetter = wordsInPhrase[0].substring(0, 1).toLowerCase();
        String firstWord = bufferLetter.concat(wordsInPhrase[0].substring(1));

        bufferLetter = wordsInPhrase[wordsInPhrase.length - 1].substring(0, 1).toUpperCase();
        String lastWord = bufferLetter.concat(wordsInPhrase[wordsInPhrase.length - 1].substring(1));

        String finalMark = lastWord.substring(lastWord.length() - 1);

        if (firstWord.indexOf(',') != -1) {
            firstWord = firstWord.substring(0, firstWord.length() - 1).concat(finalMark);
            lastWord = lastWord.substring(0, lastWord.length() - 1).concat(",");
        } else {
            firstWord = firstWord.concat(finalMark);
            lastWord = lastWord.substring(0, lastWord.length() - 1);
        }

        wordsInPhrase[0] = lastWord;
        wordsInPhrase[wordsInPhrase.length - 1] = firstWord;

        StringBuilder outputPhrase = new StringBuilder();

        for (String temp : wordsInPhrase) {
            outputPhrase.append(temp).append(' ');
        }

        return outputPhrase.toString().trim();
    }
}
