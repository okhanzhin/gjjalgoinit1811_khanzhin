package com.getjavajob.training.algo.init.khanzhino;

import java.util.Scanner;

import static java.lang.Math.sqrt;

public class TaskCh10N056 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int inputNumber = sc.nextInt();

        System.out.println(checkNumberIsPrime(inputNumber, 2));
    }

    public static boolean checkNumberIsPrime(int inputNumber, int i) {
        if (i < 2 || inputNumber < 2) {
            return false;
        } else if (i > sqrt(inputNumber) + 1) {
            return true;
        } else if (inputNumber == 2) {
            return true;
        } else if (inputNumber % i != 0) {
            return checkNumberIsPrime(inputNumber, i + 1);
        }

        return false;
    }
}
