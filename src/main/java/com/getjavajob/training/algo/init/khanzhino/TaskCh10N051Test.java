package com.getjavajob.training.algo.init.khanzhino;

import static util.Assert.assertFile;

public class TaskCh10N051Test {
    public static void main(String[] args) {
        testCompareResultsAsFiles();
    }

    public static void testCompareResultsAsFiles() {
        TaskCh10N051.main(null);
        assertFile("TaskCh10N051Test.testResultsAsFiles",
                "/src/main/java/com/getjavajob/training/algo/init/khanzhino/TaskCh10N051.ExpectedOutputs.txt",
                "/src/main/java/com/getjavajob/training/algo/init/khanzhino/TaskCh10N051.ActualOutputs.txt");
    }
}
