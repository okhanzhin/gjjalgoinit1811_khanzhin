package com.getjavajob.training.algo.init.khanzhino;

public class TaskCh04N015 {
    public static void main(String[] args) {
        System.out.println(calculateAge(6, 1985, 12, 2014));
    }

    public static int calculateAge(int birthMonth, int birthYear, int currentMonth, int currentYear) {
        return birthMonth > currentMonth ? currentYear - birthYear - 1 : currentYear - birthYear;
    }
}
