package com.getjavajob.training.algo.init.khanzhino;

import static com.getjavajob.training.algo.init.khanzhino.TaskCh10N045.getArithmeticMember;
import static com.getjavajob.training.algo.init.khanzhino.TaskCh10N045.sumArithmeticMembers;
import static util.Assert.assertEquals;

public class TaskCh10N045Test {
    public static void main(String[] args) {
        testArithmeticMember();
        testSumOfArithmeticNMembers();
    }

    public static void testArithmeticMember() {
        assertEquals("TaskCh10N045Test.testArithmeticMember", 12.0,
                      getArithmeticMember(2, 5, 3));
    }

    public static void testSumOfArithmeticNMembers() {
        assertEquals("TaskCh10N045Test.testSumOfArithmeticNMembers", 21.0,
                      sumArithmeticMembers(2, 5, 3));
    }
}
