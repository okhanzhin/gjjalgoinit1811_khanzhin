package com.getjavajob.training.algo.init.khanzhino;

import static com.getjavajob.training.algo.init.khanzhino.TaskCh12N234.deleteLine;
import static com.getjavajob.training.algo.init.khanzhino.TaskCh12N234.deleteColumn;
import static util.Assert.assertEquals;

public class TaskCh12N234Test {
    public static void main(String[] args) {
        testLineDeletion();
        testColumnDeletion();
    }

    public static void testLineDeletion() {
        assertEquals("TaskCh12N234Test.testLineDeletion", new int[][]{
                                {19, 50, 16, 18},
                                {23, 20, 19, 15},
                                {23, 20, 19, 15},
                                {0, 0, 0, 0}},
                                deleteLine(3, new int[][]{
                                {19, 50, 16, 18},
                                {23, 20, 19, 15},
                                {123, 200, 1000, 160},
                                {23, 20, 19, 15}}));
    }

    public static void testColumnDeletion() {
        assertEquals("TaskCh12N234Test.testColumnDeletion", new int[][]{
                                {19, 50, 18, 0},
                                {23, 20, 15, 0},
                                {123, 200, 160, 0},
                                {23, 20, 15, 0}},
                                deleteColumn(3, new int[][]{
                                {19, 50, 16, 18},
                                {23, 20, 19, 15},
                                {123, 200, 1000, 160},
                                {23, 20, 19, 15}}));
    }
}